import { Subscription } from 'rxjs/Subscription';
import { VodSection, ContentType, ContentName } from './../../models/vod';
import { Component, Input, ViewChild } from '@angular/core';
import { Slides, NavController, Platform } from 'ionic-angular';
import { SliderOptions, Channel } from '../../models';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

@Component({
    selector: 'slider-content',
    templateUrl: './slider-content.html'
})

export class SliderContentComponent {

    @Input() channels: Channel[];
    @Input() vod: VodSection;
    @Input() contentType: ContentType;
    @Input() title: string;
    @Input() redirect: RedirectData;
    @ViewChild('slider') slider: Slides;

    sliderOptions: SliderOptions = {
        slidesPerView: 1.4,
        spaceBetween: 15,
        freeMode: false
    };

    private subscriber: Subscription;

    constructor(private screenOrientation: ScreenOrientation, public navCtrl: NavController, public platform: Platform) {}

    ngAfterViewInit(): void {
        this.calculateSliderOptions();
        this.subscriber = this.screenOrientation.onChange().subscribe(
            () => {
                this.calculateSliderOptions();
            }
        );
    }

    public redirectTo() {
      this.navCtrl.push(this.redirect.page, { genre_id: this.redirect.genre });
    }

    public get isNeedRedirectCard(): boolean {
      if (this.redirect) {
        return true;
      }
      return false;
    }

    public get isChannels(): boolean {
        return this.contentType == ContentName.CHANNEL;
    }

    public get isVideos(): boolean {
        return this.contentType == ContentName.VIDEO;
    }

    public get isTvshows(): boolean {
        return this.contentType == ContentName.TV;
    }

    private applySliderOptions() {
        if (this.slider) {
            Object.assign(this.slider, this.sliderOptions);
            this.slider.update();
        }
    }

    private get isLandscape(): boolean {
        return this.screenOrientation.type.includes('landscape');
    }

    private get isTablet(): boolean {
        return this.platform.is('tablet');
    }

    private calculateSliderOptions() {
        if (this.isLandscape && !this.isTablet) {
            this.sliderOptions.slidesPerView = (this.contentType == ContentName.CHANNEL || this.contentType == ContentName.TV) ? 3.4 : 4.4;
        } else if (!this.isLandscape && !this.isTablet) {
            this.sliderOptions.slidesPerView = (this.contentType == ContentName.CHANNEL || this.contentType == ContentName.TV)? 1.4 : 2.4;
        } else if(this.isLandscape && this.isTablet) {
            this.sliderOptions.slidesPerView = (this.contentType == ContentName.CHANNEL || this.contentType == ContentName.TV)? 4.4 : 5.4;
        } else if(!this.isLandscape && this.isTablet) {
            this.sliderOptions.slidesPerView = (this.contentType == ContentName.CHANNEL || this.contentType == ContentName.TV)? 3.4 : 4.4;
        }
        this.applySliderOptions();
    }

    ngOnDestroy(): void {
        if (this.subscriber) this.subscriber.unsubscribe();
    }
}

interface RedirectData {
  page: string;
  genre: number;
}
