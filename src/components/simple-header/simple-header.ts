import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from './../../services/auth.service';
import { Genre } from './../../models/category';
import { HeaderButtonType, HeaderButton } from '../../models/header';

@Component({
    selector: 'simple-header',
    templateUrl: './simple-header.html'
})

export class SimpleHeaderComponent {

    @Input() headerButtons: HeaderButtonType[] = [HeaderButton.BUTTON_MENU, HeaderButton.BUTTON_SEARCH, HeaderButton.BUTTON_FAVORITE];
    @Input() categories: Genre[] = [];
    @Input() activeGenreId: number;

    static isSearchOpen: boolean = false;
    static isFavoriteOpen: boolean = false;

    isSearchActive: boolean = false;
    isFavoriteActive: boolean = false;

    public selectedCategory: Genre;

    public isShowModal: boolean = false;

    @Output() onFilterChange: EventEmitter<number> = new EventEmitter<number>();

    constructor(public navCtrl: NavController, private authService: AuthService) {}

    ngOnInit() {
        this.initActiveIcons();
        if (this.categories.length > 0) {
          if (this.activeGenreId) {
            this.selectedCategory = this.categories.find(c => c.id === this.activeGenreId);
          } else {
            this.selectedCategory = this.categories[0];
          }
        }
    }

    static clearAllButtonsActive(): void {
        SimpleHeaderComponent.isSearchOpen = false;
        SimpleHeaderComponent.isFavoriteOpen = false;
    }

    private initActiveIcons(): void {
        this.isSearchActive = SimpleHeaderComponent.isSearchOpen;
        this.isFavoriteActive = SimpleHeaderComponent.isFavoriteOpen;
    }

    public toggleSearch(): void {
        if (!SimpleHeaderComponent.isSearchOpen) {
            this.navCtrl.push('SearchPage', {}, {animate: true, direction: 'back'});
        } else {
            this.navCtrl.popToRoot({animate: true, direction: 'forward'});
        }
        SimpleHeaderComponent.isSearchOpen = !SimpleHeaderComponent.isSearchOpen;
        SimpleHeaderComponent.isFavoriteOpen = false;
    }

    public toggleFavorite(): void {
        if (!SimpleHeaderComponent.isFavoriteOpen) {
            this.navCtrl.push('FavoritePage', {}, {animate: true, direction: 'back'});
        } else {
            this.navCtrl.popToRoot({animate: true, direction: 'forward'});
        }
        SimpleHeaderComponent.isFavoriteOpen = !SimpleHeaderComponent.isFavoriteOpen;
        SimpleHeaderComponent.isSearchOpen = false;
    }

    public isShowButton(name: string): boolean {
        return this.headerButtons.some(button => button == name);
    }

    public goBack(): void {
        if (this.navCtrl.canGoBack()) {
            this.navCtrl.pop({animate: true, animation: 'right', duration: 150});
        } else {
            this.navCtrl.setRoot('HomePage');
        }
    }

    public filterChange(item: Genre): void {
      if (!item) {
        this.isShowModal = false;
      } else {
        this.isShowModal = false;
        this.selectedCategory = item;
        this.onFilterChange.next(item.id);
      }
    }

    public get isLogin(): boolean {
        return this.authService.isLogin;
    }

    // new

    public showModal() {
      this.isShowModal = true;
    }

}
