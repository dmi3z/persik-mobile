import { TimeService } from './../../../services/time.service';
import { Component, Input } from '@angular/core';
import { UserSubscription } from './../../../models/user';
import * as moment from 'moment';

@Component({
    selector: 'subscription-item',
    templateUrl: './subscription-item.html'
})

export class SubscriptionItemComponent {

    @Input() subscription: UserSubscription;

    public createDate: string = '';
    public expiredDate: string = '';
    public price: number = 0;

    static currentSubscription: UserSubscription;
    static selectedSubscription: UserSubscription;

    constructor(private timeService: TimeService) {}

    ngOnInit() {
        this.createDate = this.timeService.convertToDate(this.subscription.created_at);
        this.expiredDate = this.timeService.convertToDate(this.subscription.expired_at);
    }

    public isActive(): boolean {
        return moment().unix() < moment(this.subscription.expired_at).unix();
    }

    public get relativeTime(): string {
        return moment(this.subscription.expired_at).endOf('day').fromNow().split(' ')[1];
    }

    public get isShowDetails(): boolean {
        return SubscriptionItemComponent.currentSubscription == this.subscription;
    }

    public toggleDetails(): void {
        if (this.isShowDetails) {
            SubscriptionItemComponent.currentSubscription = null;
        } else {
            SubscriptionItemComponent.currentSubscription = this.subscription;
        }
    }

    public showDetails(): void {
      SubscriptionItemComponent.currentSubscription = this.subscription;
    }

    public get isCurrentSubscriptionSelected(): boolean {
        return SubscriptionItemComponent.selectedSubscription == this.subscription;
    }

    public selectSubscription(): void {
        SubscriptionItemComponent.selectedSubscription = this.subscription;
    }

}
