import { Component } from '@angular/core';
import { UserSubscription, Products } from '../../models/user';
import { AuthService } from './../../services/auth.service';

@Component({
    selector: 'subscriptions',
    templateUrl: './subscriptions.html'
})

export class SubscriptionsComponent {

    public userSubscriptions: UserSubscription[] = [];
    public products: Products = new Products();

    constructor(private authService: AuthService) {}

    ngOnInit() {
        this.getUserData();
    }

    public get isHaveSubscriptions(): boolean {
        return this.userSubscriptions.length > 0;
    }

    private getUserData(): void {
        this.authService.getUserSubscriptions().then(res => {
            if (res && res.length > 0) {
                this.userSubscriptions = res;
            }
        });
        this.authService.getTariffs().then(res => this.products = res);
    }
}
