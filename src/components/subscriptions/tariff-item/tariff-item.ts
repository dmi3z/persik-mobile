import { Tariff } from './../../../models/user';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'tariff-item',
    templateUrl: './tariff-item.html'
})

export class TariffItemComponent {

    @Input() tariff: Tariff;

    static selectedOptionId: number = 0;
    static selectedTariffId: number;
    static showDetailsTariffId: number;

    constructor() {}

    ngOnInit() {}

    public get isCurrentTariffSelected(): boolean {
        return TariffItemComponent.selectedTariffId == this.tariff.product_id;
    }

    public isCurrentOptionSelected(optionId: number): boolean {
        return TariffItemComponent.selectedOptionId == optionId;
    }

    public selectTariff(): void {
        TariffItemComponent.selectedTariffId = this.tariff.product_id;
        if (this.tariff.options.length > 0) {
            TariffItemComponent.selectedOptionId = this.tariff.options[0].product_option_id;
        } else {
            TariffItemComponent.selectedOptionId = null;
        }
    }

    public selectOption(optionId: number): void {
        TariffItemComponent.selectedTariffId = this.tariff.product_id;
        TariffItemComponent.selectedOptionId = optionId;
    }

    public get isShowDetails(): boolean { // Возвращает раскрыты ли сейчас опции тарифа
        return TariffItemComponent.showDetailsTariffId == this.tariff.product_id;
    }

    public toggleDetails(): void { // Делает тоггл опций тарифа
        if (this.isShowDetails) {
            TariffItemComponent.showDetailsTariffId = null;
        } else {
            TariffItemComponent.showDetailsTariffId = this.tariff.product_id;
        }
    }

    public showDetails(): void {
      TariffItemComponent.showDetailsTariffId = this.tariff.product_id;
    }
}
