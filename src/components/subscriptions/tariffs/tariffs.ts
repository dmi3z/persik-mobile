import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TariffItemComponent } from './../tariff-item/tariff-item';
import { Products, Tariff, TariffOption } from './../../../models/user';

@Component({
    selector: 'tariffs',
    templateUrl: './tariffs.html'
})

export class TariffsComponent {

    @Input() products: Products;

    constructor(public navCtrl: NavController) {}

    ngOnInit() {}

    public get isHaveSelectedTariff(): boolean {
        if (TariffItemComponent.selectedTariffId && TariffItemComponent.selectedOptionId) return true;
        return false;
    }

    public get purchaseSumm(): number {
        if (this.isHaveSelectedTariff) {
            return this.products.products
                .find(p => p.product_id == TariffItemComponent.selectedTariffId).options
                .find(o => o.product_option_id == TariffItemComponent.selectedOptionId)
                .price;
        } else {
            return 0;
        }       
    }
    
    public getSubscription(): void {
        if (this.isHaveSelectedTariff) {
            const selectedTariff: Tariff = this.products.products.find(p => p.product_id == TariffItemComponent.selectedTariffId);
            const selectedOption: TariffOption = selectedTariff.options.find(o => o.product_option_id == TariffItemComponent.selectedOptionId);
            selectedTariff.options = [];
            selectedTariff.options.push(selectedOption);
            this.navCtrl.push('BillingConfirmPage', { tariff: selectedTariff });
        }
    }
}
