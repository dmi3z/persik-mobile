import { SubscriptionItemComponent } from './../subscription-item/subscription-item';
import { NavController } from 'ionic-angular';
import { UserSubscription } from './../../../models/user';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'user-subscriptions',
    templateUrl: './user-subscriptions.html'
})

export class UserSubscriptionsComponent {

    @Input() subscriptions: UserSubscription[];

    constructor(public navCtrl: NavController) {}

    public get totalSumm(): number {
        let totalSumm: number = 0;
        /* this.subscriptions.filter(s => s.is_selected).forEach(s => {
            totalSumm += +s.cost;
        }); */
        if (this.isHaveSelected) {
            totalSumm = SubscriptionItemComponent.selectedSubscription.cost;
        }
        return totalSumm;
    }

    public get isHaveSelected(): boolean {
        // return this.subscriptions.filter(s => s.is_selected).length > 0; -- new
        if (SubscriptionItemComponent.selectedSubscription) return true;
        return false;
    }

    public extendSubscriptions() {
        if (this.isHaveSelected) {
            // const u_subs: UserSubscription[] = this.subscriptions.filter(s => s.is_selected);
            const u_subs: UserSubscription = SubscriptionItemComponent.selectedSubscription;
            this.navCtrl.push('BillingConfirmPage', { user_subscriptions: u_subs });
        }
    }

}
