import { Video } from './../../models/vod';
import { Subscription } from 'rxjs/Subscription';
import { Component, Input, SimpleChanges } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Tvshow } from './../../models/tvshow';
import { Channel } from './../../models/channel';
import { DataService } from './../../services/data.service';
import { ImageService } from './../../services/image.service';
import { TimeService } from './../../services/time.service';
import { ContentName } from '../../models/vod';


@Component({
    selector: 'tvshow-tape-item',
    templateUrl: './tvshow-tape-item.html'
})

export class TvshowTapeItemComponent {

    @Input() tvshow: Tvshow;
    @Input() channel: Channel;

    public progress: number = 0;

    private timeSubscriber: Subscription;

    constructor(
        private timeService: TimeService,
        private imageService: ImageService,
        private dataService: DataService,
        private navCtrl: NavController
    ) {}

    ngOnInit() {
        this.calculateProgress();
        this.timeSubscriber = this.timeService.timeController.subscribe(() => {
            this.calculateProgress();
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        /* this.tvshowThumbnail.then(c => {
            Object.assign(this.tvshow, { cover: c });
        }); */
        if (changes.tvshow) {
            this.imageService.loadTvshowThumbnail(this.tvshow);
        }
    }

    public get logo(): string {
        return this.channel.logo;
    }

    public get startTime(): string {
        return this.timeService.convertToTime(this.tvshow.start);
    }

    public get isCurrent(): boolean {
        const currentTime = this.timeService.currentTime;
        return this.tvshow.start <= currentTime && this.tvshow.stop > currentTime;
    }

    public get isArchive(): boolean {
        const currentTime: number = this.timeService.currentTime;
        return this.tvshow.start >= (currentTime - this.channel.dvr_sec) && this.tvshow.stop < currentTime;;
    }

    private calculateProgress(): void {
        if (this.isCurrent) {
            const currentTime: number = this.timeService.currentTime;
            this.progress = ((currentTime - this.tvshow.start) / (this.tvshow.stop - this.tvshow.start)) * 100;
        } else {
            this.progress = 0;
        }
    }

    public showInfoPage(): void {
        if (!this.isCurrent) {
            this.dataService.getVideoInfo(+this.tvshow.video_id).then(video => {
                this.checkCover(video);
                this.navCtrl.push(
                    'VodInfoPage',
                        {
                            info: video,
                            type: ContentName.TV,
                            tvshow_id: this.tvshow.tvshow_id,
                            video_id: this.tvshow.video_id
                        }
                    );
            });
        }
    }

    private checkCover(video: Video): void {
      if (video) {
        if (!video.cover || video.cover.length == 0) {
            video.cover = this.tvshow.cover;
        }
      }

    }

    ngOnDestroy() {
        if (this.timeSubscriber) this.timeSubscriber.unsubscribe();
    }
}
