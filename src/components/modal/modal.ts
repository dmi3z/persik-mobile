
import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';import { Subscription } from 'rxjs/Subscription';
import { App } from 'ionic-angular';
import { AuthService } from './../../services/auth.service';
import { FavoriteService } from './../../services/favorite.service';
import { ModalSettings } from './../../models/modal';
import { ModalController } from './../../services/modal.controller';
import { Observable } from 'rxjs/Observable';
import { ContentName } from '../../models/vod';

@Component({
    selector: 'modal',
    templateUrl: './modal.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ModalComponent {

    public isShowModal: boolean = false;
    private modalSettings: ModalSettings;

    private modalStateSubscriber: Subscription;

    constructor(
        private modalCtrl: ModalController, 
        private favoriteService: FavoriteService,
        private authService: AuthService,
        public app: App,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.modalStateSubscriber = this.modalCtrl.modalState.subscribe((data: ModalSettings) => {
            if  (data.id == -1) { 
                this.isShowModal = false;
            } else {
                this.modalSettings = data;
                this.isShowModal = true;
                this.cdr.detectChanges();
            }
        });
    }

    public closeModal(): void {
        this.modalCtrl.dismiss();
    }

    public stopProp(event): void {
        event.stopPropagation();    
    }

    public get isFavorite(): Observable<boolean> {
        switch (this.modalSettings.type) {
            case ContentName.CHANNEL:
                return this.favoriteService.isChannelFavorite(+this.modalSettings.id);
            case ContentName.TV:
                return this.favoriteService.isTvshowFavorite(this.modalSettings.id.toString());
            case ContentName.VIDEO:
                return this.favoriteService.isVideoFavorite(+this.modalSettings.id);
            default:
                break;
        }
    }

    public get isNeedDetails(): boolean {
        return this.modalSettings.type != ContentName.CHANNEL;
    }

    public get isLogin(): boolean {
        return this.authService.isLogin;
    }

    public addToFavorite() {
        this.favoriteService.addToFavorite(this.modalSettings);
        this.closeModal();
    }

    public removeFromFavorite() {
        this.favoriteService.deleteFromFavorite(this.modalSettings);
        this.closeModal();
    }

    public showDetails() {
        if (this.modalSettings.type == ContentName.TV || this.modalSettings.type == ContentName.VIDEO) {
            this.app.getActiveNav().push(
                'VodInfoPage',
                { 
                    info: this.modalSettings.info,
                    genres: this.modalSettings.genres,
                    type: this.modalSettings.type,
                    tvshow_id: this.modalSettings.type == ContentName.TV ? this.modalSettings.id : null,
                    video_id: this.modalSettings.type == ContentName.VIDEO ? this.modalSettings.id : null,
                } );
            this.closeModal();
        }
    }

    ngOnDestroy() {
        if (this.modalStateSubscriber) this.modalStateSubscriber.unsubscribe();
    }

}