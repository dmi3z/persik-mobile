import { ContentType, ContentName } from './../../models/vod';
import { AuthService } from './../../services/auth.service';
import { Subscription } from 'rxjs/Subscription';
import { FavoriteService } from './../../services/favorite.service';
import { Observable } from 'rxjs/Observable';
import { ModalController } from './../../services/modal.controller';
import { NavController } from 'ionic-angular';
import { ImageService } from './../../services/image.service';
import { Genre } from './../../models/category';
import { DataService } from './../../services/data.service';
import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Video } from '../../models/vod';
import { VodState } from '../../redux/vod/vod.state';
import { Store } from '@ngrx/store';
import { ModalSettings } from '../../models/modal';

@Component({
  selector: 'video-card',
  templateUrl: './video-card.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class VideoCardComponent {

  @Input() content: any; // video_id, tvshow_id
  @Input() type: ContentType = ContentName.VIDEO;

  public videoInformation: Video = new Video();
  public videoGenres: string[] = [];

  private vodStoreSubscriber: Subscription;

  constructor(
    private dataService: DataService,
    private vodStore: Store<VodState>,
    private imageService: ImageService,
    public navCtrl: NavController,
    private modalCtrl: ModalController,
    private favoriteService: FavoriteService,
    private authService: AuthService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    if (this.content.video_id) {
      this.dataService.getVideoInfo(this.content.video_id).then(res => {
        this.videoInformation = res;
        this.loadGenres();
        this.cdr.detectChanges();
      });
    } else if (this.content.tvshow_id) {
      this.dataService.getTvshowInfo(this.content.tvshow_id).then(res => {
        this.dataService.getVideoInfo(+res.video_id).then(info => {
          this.videoInformation = info;
          if (!this.videoInformation.cover) this.videoInformation.cover = this.imageService.getChannelFrame(res.channel_id, res.start + (res.stop - res.start) / 2);
          this.loadGenres();
          this.cdr.detectChanges();
        });
      });
    }
  }

  public get isTvshow(): boolean {
    return this.type == ContentName.TV;
  }

  private loadGenres(): void { // Получение списка жанров для видео по id жанров
    this.vodStoreSubscriber = this.vodStore.select('vodCategories').subscribe(data => {
      const allGenresArray = data.categories.map(cat => cat.genres);
      let allGenres: Genre[] = [];
      allGenresArray.forEach(genre => {
        allGenres.push(...genre);
      });

      this.videoGenres = allGenres.filter(genre => {
        return this.videoInformation.genres.some(genreId => genreId == genre.id);
      }).map(res => {
        let name = res.name;
        let part = name.split(')');
        if (part[1]) return part[1];
        return name;
      });
    })
  }

  public showInfoPage(): void {
    this.navCtrl.push(
      'VodInfoPage',
      {
        info: this.videoInformation,
        genres: this.videoGenres,
        type: this.content.tvshow_id ? ContentName.TV : ContentName.VIDEO,
        tvshow_id: this.content.tvshow_id ? this.content.tvshow_id : null,
        video_id: this.content.video_id ? this.content.video_id : null
      }
    );
  }

  public showModal(): void {
    if (this.content.video_id) this.modalCtrl.create(new ModalSettings(this.content.video_id, ContentName.VIDEO, this.videoInformation, this.videoGenres));
    if (this.content.tvshow_id) this.modalCtrl.create(new ModalSettings(this.content.tvshow_id, ContentName.TV, this.videoInformation, this.videoGenres));
  }

  public get isFavorite(): Observable<boolean> {
    if (this.content.video_id) return this.favoriteService.isVideoFavorite(this.content.video_id);
    if (this.content.tvshow_id) return this.favoriteService.isTvshowFavorite(this.content.tvshow_id);
  }

  public removeFavorite(event): void {
    event.stopPropagation();
    const type: ContentType = this.content.tvshow_id ? ContentName.TV : ContentName.VIDEO;
    const id: any = this.content.tvshow_id ? this.content.tvshow_id : this.content.video_id;
    this.favoriteService.deleteFromFavorite(new ModalSettings(id, type));
  }

  public get isLogin(): boolean {
    return this.authService.isLogin;
  }

  ngOnDestroy() {
    if (this.vodStoreSubscriber) this.vodStoreSubscriber.unsubscribe();
  }

}
