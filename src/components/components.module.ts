import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

import {
  SimpleHeaderComponent,
  VideoCardComponent,
  ChannelComponent,
  ChannelOnairComponent,
  AccountInfoComponent,
  SubscriptionsComponent,
  UserSubscriptionsComponent,
  TariffsComponent,
  TariffItemComponent,
  SubscriptionItemComponent,
  SliderContentComponent,
  PlayerComponent,
  ChannelPlayerControllerComponent,
  VideoPlayerControllerComponent,
  TvshowItemComponent,
  TvshowTapeItemComponent,
  ProgressLineComponent,
  ModalFilterComponent,
  ModalAdultComponent
} from './index';
import { CapitalizePipe } from '../pipes/capitalize.pipe';

@NgModule({
  declarations: [
    SimpleHeaderComponent,
    VideoCardComponent,
    ChannelComponent,
    ChannelOnairComponent,
    AccountInfoComponent,
    SubscriptionsComponent,
    UserSubscriptionsComponent,
    TariffsComponent,
    TariffItemComponent,
    SubscriptionItemComponent,
    SliderContentComponent,
    PlayerComponent,
    ChannelPlayerControllerComponent,
    VideoPlayerControllerComponent,
    TvshowItemComponent,
    TvshowTapeItemComponent,
    ProgressLineComponent,
    ModalFilterComponent,
    CapitalizePipe,
    ModalAdultComponent
  ],
  imports: [
    IonicModule
  ],
  exports: [
    SimpleHeaderComponent,
    VideoCardComponent,
    ChannelComponent,
    ChannelOnairComponent,
    AccountInfoComponent,
    SubscriptionsComponent,
    UserSubscriptionsComponent,
    TariffsComponent,
    TariffItemComponent,
    SubscriptionItemComponent,
    SliderContentComponent,
    PlayerComponent,
    ChannelPlayerControllerComponent,
    VideoPlayerControllerComponent,
    TvshowItemComponent,
    TvshowTapeItemComponent,
    ProgressLineComponent,
    ModalFilterComponent,
    CapitalizePipe,
    ModalAdultComponent
  ]
})

export class ComponentsModule {}
