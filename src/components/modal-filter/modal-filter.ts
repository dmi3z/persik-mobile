import { Genre } from './../../models/category';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'modal-filter',
  templateUrl: './modal-filter.html'
})

export class ModalFilterComponent {

  @Input() categories: Genre[];
  @Input() selectedCategory: Genre;
  @Output() selectCategory: EventEmitter<Genre> = new EventEmitter();

  constructor() {}

  public changeCategory(item: Genre) {
    this.selectCategory.emit(item);
  }

  public closeModal(event: Event): void {
    event.stopPropagation();
    this.selectCategory.emit();
  }

  public isCurrentGenre(genreId: number): boolean {
    return genreId == this.selectedCategory.id;
  }

}
