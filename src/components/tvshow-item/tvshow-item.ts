import { ImageService } from './../../services/image.service';
import { AuthService } from './../../services/auth.service';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Channel } from './../../models/channel';
import { DataService } from './../../services/data.service';
import { Video, ContentType, ContentName } from './../../models/vod';
import { FavoriteService } from './../../services/favorite.service';
import { Tvshow } from './../../models/tvshow';
import { TimeService } from './../../services/time.service';
import { ModalSettings } from '../../models/modal';

@Component({
    selector: 'tvshow-item',
    templateUrl: './tvshow-item.html'
})

export class TvshowItemComponent {

    @Input() tvshow: Tvshow;
    @Input() currentChannel: Channel;
    @Output() redirectEvent: EventEmitter<RedirectEventType> = new EventEmitter();

    public progress: number = 0;

    private subscriber: Subscription;

    constructor(
        private timeService: TimeService,
        private favoriteService: FavoriteService,
        private dataService: DataService,
        public navCtrl: NavController,
        private authservice: AuthService,
        private imageService: ImageService
    ) {}

    ngOnInit() {
        this.updateProgressLine();
        this.subscriber = this.timeService.timeController.subscribe(() => {
            this.updateProgressLine();
        });
    }

    public get isCurrentTvshow() {
        const time = this.timeService.currentTime;
        return this.tvshow.start <= time && this.tvshow.stop > time; 
    }

    public showTvshow(): void {
        if (!this.isCurrentTvshow) {
            this.dataService.getVideoInfo(+this.tvshow.video_id).then(data => {
                if (!data.cover || data.cover.length == 0) {
                    data.cover = this.imageService.getTvshowFrame(this.tvshow);
                }
                this.redirectEvent.next({
                    info: data,
                    type: ContentName.TV,
                    video_id: +this.tvshow.video_id,
                    tvshow_id: this.tvshow.tvshow_id
                });
            });
        }
    }

    public convertToTime() {
        return this.timeService.convertToTime(this.tvshow.start);
    }

    public removeFavorite(event): void {
        event.stopPropagation();
        this.favoriteService.deleteFromFavorite(new ModalSettings(this.tvshow.tvshow_id, ContentName.TV));
    }

    public get isFavorite(): Observable<boolean> {
        return this.favoriteService.isTvshowFavorite(this.tvshow.tvshow_id);
    }

    public get isArchive(): boolean {
        return this.tvshow.start >= (this.timeService.currentTime - this.deepArchive) && this.tvshow.stop < this.timeService.currentTime;
    }

    public get isLogin(): boolean {
        return this.authservice.isLogin;
    }

    private get deepArchive(): number {
        return this.currentChannel.dvr_sec;
    }

    private updateProgressLine() {
        if (this.isCurrentTvshow) { 
            this.progress = ((this.timeService.currentTime - this.tvshow.start) / (this.tvshow.stop - this.tvshow.start)) * 100;
        }
    }

    ngOnDestroy() {
        if (this.subscriber) this.subscriber.unsubscribe();
    }

}

interface RedirectEventType {
    info: Video;
    type: ContentType;
    video_id: number;
    tvshow_id: string;
}