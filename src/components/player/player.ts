import { Component, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import * as Hls from 'hls.js';
import { NavController } from 'ionic-angular';

import { ContentType, ContentName } from './../../models/vod';
import { TimeService } from './../../services/time.service';
import { ImageService } from './../../services/image.service';
import { DataService } from './../../services/data.service';
import { AuthService } from './../../services/auth.service';
import { PlayerEvents } from '../../models/player';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

@Component({
  selector: 'player',
  templateUrl: './player.html'
})

export class PlayerComponent {

  @ViewChild('video') video: ElementRef;

  public isShowLoginError: boolean = false;
  public isShowSubscribeError: boolean = false;
  public errorMessage: string = '';
  public contentPoster: string;
  public playerEvents: EventEmitter<string> = new EventEmitter<string>();

  private hls: any;
  private player: HTMLVideoElement;

  private course_id: number;

  constructor(
    private authService: AuthService,
    private dataService: DataService,
    private imageService: ImageService,
    private timeService: TimeService,
    public navCtrl: NavController,
    private screenOrientation: ScreenOrientation
  ) { }

  ngOnInit() {
    this.screenOrientation.unlock();
    this.player = this.video.nativeElement;
    this.hls = new Hls();
    document.addEventListener("resume", this.resume.bind(this), false); // Подписки на событие перехода устройства в спящий режим
    document.addEventListener("pause", this.pause.bind(this), false);
  }


  public play(id: number | string, type: ContentType) {
    if (this.isAuthorized) {
      this.isShowLoginError = false;
      switch (type) {
        case ContentName.CHANNEL:
          this.playChannel(+id);
          break;

        case ContentName.VIDEO:
          this.playVideo(+id);
          break;

        case ContentName.TV:
          this.playTvshow(id.toString());
          break;

        case 'tvshow':
          this.playTvshow(id.toString());
          break;
        default:
          break;
      }
    } else {
      this.contentPoster = this.imageService.getChannelFrame(+id, this.timeService.currentTime);
      this.errorMessage = `Для просмотра ${type == 'channel' ? 'каналов' : 'видео'} необходимо авторизоваться`;
      this.isShowLoginError = true;
      this.playerEvents.emit(PlayerEvents.PLAYER_ERROR_LOGIN);
    }
  }

  public stop() {
    if (this.hls) {
      this.hls.stopLoad();
      this.hls.detachMedia();
      this.destroyHls();
    }
  }

  public seek(value: number) {
    this.player.currentTime = value;
  }

  public get volume() {
    return this.player.volume;
  }

  public set volume(level) {
    this.player.volume = level;
  }

  public get duration(): number {
    return this.player.duration;
  }

  public get currentTime(): number {
    return this.player.currentTime;
  }

  public redirectToAuthPage(): void {
    this.navCtrl.push('LoginPage');
  }

  public redirectToSubscribePage(): void {
    if (this.course_id) {
      this.navCtrl.push('CoursePaymentPage', { id: this.course_id });
    } else {
      this.navCtrl.push('AccountPage', { page: 'subscription' });
    }
  }

  public resume(): void {
    this.player.play();
  }

  public pause(): void {
    this.player.pause();
  }

  private destroyHls() {
    if (this.hls) {
      this.hls.destroy();
      const tmpHls = this.hls;
      setTimeout(() => {
        if (tmpHls.abrController && tmpHls.abrController._bwEstimator) {
          tmpHls.abrController._bwEstimator.hls = null;
        }
        if (tmpHls.coreComponents && tmpHls.coreComponents.length) {
          tmpHls.coreComponents.forEach((component) => {
            component.hls = null;
            component.media = null;
            component.cea608Parser = null;
          });
        }
        if (tmpHls.networkControllers && tmpHls.networkControllers.length) {
          tmpHls.networkControllers.forEach((component) => {
            component.hls = null;
          });
        }
        tmpHls.coreComponents = null;
        tmpHls.networkControllers = null;
      }, 5000);
      this.hls = null;
    }
  }

  private playChannel(id: number): void {
    this.contentPoster = this.imageService.getChannelFrame(+id, this.timeService.currentTime);
    this.isShowSubscribeError = false;
    this.dataService.getChannelStream(id).then(res => {
      this.playUrl(res.stream_url);
    }).catch(() => {
      this.playerEvents.emit(PlayerEvents.PLAYER_ERROR_SUBSCRIPTION); // Нет подписки
      this.isShowSubscribeError = true;
      this.errorMessage = 'Необходимо приобрести подписку';
    });
  }

  private playTvshow(id: string) {
    this.dataService.getTvshowStream(id).then(res => {
      this.playUrl(res.stream_url);
    }).catch(err => {
      if (err.status == 403) {
        this.isShowSubscribeError = true;
        this.errorMessage = 'Необходимо приобрести подписку';
        this.contentPoster = '';
        this.playerEvents.emit(PlayerEvents.PLAYER_ERROR_SUBSCRIPTION);
      }
    });
  }

  private playVideo(id: number): void {
    this.dataService.getVideoStream(id).then(res => {
      this.playUrl(res.stream_url);
    }).catch(err => {
      if (err.status == 403) {
        this.dataService.getVideoInfo(id).then(info => {
          if (info && info.category_id === 6) {
            this.course_id = info.in_products[0].product_options[0].option_id;
          }
        });
        this.isShowSubscribeError = true;
        this.errorMessage = 'Необходимо приобрести подписку';
        this.contentPoster = '';
        this.playerEvents.emit(PlayerEvents.PLAYER_ERROR_SUBSCRIPTION);
      }
    });;
  }

  private onPlayerStateChange(): void {
    this.playerEvents.emit(PlayerEvents.PLAYER_READY);
  }

  private onPlayerPause(): void {
    this.playerEvents.emit(PlayerEvents.PLAYER_PAUSE);
  }

  private onPlayerPlay(): void {
    this.playerEvents.emit(PlayerEvents.PLAYER_PLAY);
  }

  private playUrl(url: string) {
    this.hls = new Hls();
    this.hls.loadSource(url);
    this.hls.attachMedia(this.player);
    this.hls.on(Hls.Events.MANIFEST_PARSED, () => {
      this.player.play();
      this.player.addEventListener('loadedmetadata', this.onPlayerStateChange.bind(this));
      this.player.addEventListener('play', this.onPlayerPlay.bind(this));
      this.player.addEventListener('pause', this.onPlayerPause.bind(this));
    });
    this.hls.on(Hls.Events.ERROR, function (e, data) {
      if (data.fatal) {
        switch (data.type) {
          case Hls.ErrorTypes.NETWORK_ERROR:
            // fatal network error encountered, try to recover
            break;
          case Hls.ErrorTypes.MEDIA_ERROR:
            // fatal media error encountered, try to recover
            break;
          default:
            // hls destroy
            this.hls.destroy();
            break;
        }
      }
    }
    );
  }

  private get isAuthorized(): boolean {
    return this.authService.isLogin;
  }

  ngOnDestroy() {
    this.destroyHls();
    this.player.removeEventListener('loadedmetadata', this.onPlayerStateChange.bind(this));
    this.player.removeEventListener('play', this.onPlayerPlay.bind(this));
    this.player.removeEventListener('pause', this.onPlayerPause.bind(this));
    document.removeEventListener("resume", this.resume.bind(this), false);
    document.removeEventListener("pause", this.pause.bind(this), false);
  }

}
