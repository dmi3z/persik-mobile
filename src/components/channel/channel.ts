import { Subscription } from 'rxjs/Subscription';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Channel } from '../../models';
import { ImageService } from '../../services/image.service';
import { Tvshow } from '../../models/tvshow';
import { ModalSettings } from '../../models/modal';
import { ContentName } from '../../models/vod';
import { AuthService } from './../../services/auth.service';
import { ModalController } from './../../services/modal.controller';
import { FavoriteService } from './../../services/favorite.service';
import { TimeService } from './../../services/time.service';
import { DataService } from './../../services/data.service';

@Component({
    selector: 'channel',
    templateUrl: './channel.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ChannelComponent {

    @Input() channel: Channel;
    public thumbnail: string = '';
    public tvshow: Tvshow = new Tvshow();
    public progress: number = 0;

    private timeSubscriber: Subscription;

    constructor(
        private imageService: ImageService,
        private dataService: DataService,
        private timeService: TimeService,
        private favoriteService: FavoriteService,
        private modalCtrl: ModalController,
        public navCtrl: NavController,
        private authService: AuthService,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit(): void {
        this.getChannelFrame();
        this.loadCurrentTvShow();
        this.timeSubscriber = this.timeService.timeController.subscribe(() => {
            this.getProgress();
        });
    }

    public get isFavorite(): Observable<boolean> {
        return this.favoriteService.isChannelFavorite(this.channel.channel_id);
    }

    public showModal(): void {
        this.modalCtrl.create(new ModalSettings(this.channel.channel_id, ContentName.CHANNEL));
    }

    public openChannel(): void {
        this.navCtrl.push('ChannelPlayerPage', { channel_id: this.channel.channel_id });
    }

    public get isLogin(): boolean {
        return this.authService.isLogin;
    }

    public removeFavorite(event): void {
        event.stopPropagation();
        this.favoriteService.deleteFromFavorite(new ModalSettings(this.channel.channel_id, ContentName.CHANNEL));
    }

    public get isAdult(): boolean {
      return this.channel.age_rating == '18+';
    }

    public get channelLogo(): string {
      return this.imageService.getChannelLogo(this.channel.channel_id);
    }

    public get imageStub(): string {
      return this.imageService.getChannelFrame(30, this.timeService.currentTime, 'crop', 256, 144);
    }

    private loadCurrentTvShow(): void {
        this.dataService.getCurrentTvShow(this.channel.channel_id).then(res => {
            this.tvshow = res;
            this.getProgress();
        });
        /* this.tvshowsStore.select('tvshowsData').subscribe(tvshows => {
            let channelTvshows: Tvshow[] = tvshows.tvshows.filter(tvshow => tvshow.channel_id == this.channel.channel_id);
            const tvshow = channelTvshows.find(tvshow => {
                return (tvshow.start < this.timeService.currentTime && tvshow.stop > this.timeService.currentTime)
            });
            if (tvshow) {
                this.tvshow = tvshow;
            } else {
                this.tvshow = new Tvshow();
            }
            this.getProgress();
        }) */
    }

    private getChannelFrame() {
        this.thumbnail = this.imageService.getChannelFrame(this.channel.channel_id, this.timeService.currentTime, 'crop', 256, 144);
    }


    private getProgress() {
        if (this.tvshow.start && this.tvshow.stop) {
            this.progress = Math.round(((this.timeService.currentTime - this.tvshow.start)*100)/(this.tvshow.stop - this.tvshow.start));
        } else {
            this.progress = 0;
        }
        if (this.tvshow.stop <= this.timeService.currentTime) {
            this.loadCurrentTvShow();
        }
        this.cdr.detectChanges();
    }

    ngOnDestroy() {
        if (this.timeSubscriber) this.timeSubscriber.unsubscribe();
    }

}
