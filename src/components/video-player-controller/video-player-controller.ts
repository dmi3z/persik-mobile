import { Subscription } from 'rxjs/Subscription';
import { OrientationService } from './../../services/orientation.service';
import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { ControlEvents, VideoControlEventInfo } from '../../models/player';
import { TimeService } from './../../services/time.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation';


@Component({
    selector: 'video-player-controller',
    templateUrl: './video-player-controller.html'
})

export class VideoPlayerControllerComponent {

    @Input() duration: number;
    @Input() currentTime: number;
    @Input() isPlaying: boolean;
    @Input() isCanPlay: boolean;

    @Output() controlEvents: EventEmitter<VideoControlEventInfo> = new EventEmitter<VideoControlEventInfo>();

    @ViewChild('tracker_progress') tracker: ElementRef;

    private showControlsTimeout: number = 4000; // время отображение контролов в мс.
    private controlTimer: any;
    private orientationSubscriber: Subscription;
    private timeServiceSubscriber: Subscription;

    public controlPosition: number = 0;
    public progress: number = 0;

    constructor(
        private timeService: TimeService,
        private screenOrientation: ScreenOrientation,
        private orientationService: OrientationService
    ) {}

    ngOnInit() {
        this.showControls();

        this.orientationSubscriber = this.orientationService.onChange.subscribe(() => {
            this.screenOrientation.unlock();
        });

        this.timeServiceSubscriber = this.timeService.timeControllerFast.subscribe(() => {
            this.progress = this.getProgress();
        });
    }

    public showControls(): void {
        this.controlPosition = 0;
        clearTimeout(this.controlTimer);
        this.controlTimer = setTimeout(() => {
            this.controlPosition = 5; // 5 - высота контрола
        }, this.showControlsTimeout);
    }

    public pause(): void {
        this.controlEvents.next(new VideoControlEventInfo(ControlEvents.CONTROL_PAUSE));
    }

    public play(): void{
        this.controlEvents.next(new VideoControlEventInfo(ControlEvents.CONTROL_PLAY));
    }

    public get durationString(): string {
        return this.timeService.getDurationFromTime(this.duration);
    }

    public get currentTimeString(): string {
        return this.timeService.getDurationFromTime(this.currentTime);
    }

    public getProgress(): number {
        if (this.duration && this.duration != 0) {
            return Math.round((this.currentTime / this.duration) * 100);
        } else {
            return 0;
        }
    }

    public seekTo(event) {
        const percent: number = event.value / 100;
        const secondSeekPosition: number = percent * this.duration;
        this.controlEvents.next(new VideoControlEventInfo(ControlEvents.CONTROL_SEEK, secondSeekPosition));
    }

    public get isControlShows(): boolean {
        return this.controlPosition == 0;
    }

    public toggleOrientation() {
        if (this.isLandscape) {
            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        } else {
            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
        }
    }

    private get isLandscape(): boolean {
        return this.screenOrientation.type.includes('landscape');
    }

    ngOnDestroy() {
        if (this.orientationSubscriber) this.orientationSubscriber.unsubscribe();
        if (this.timeServiceSubscriber) this.timeServiceSubscriber.unsubscribe();
    }
}

