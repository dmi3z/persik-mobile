import { NavController } from 'ionic-angular';
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './../../services/auth.service';
import { UserInfo } from '../../models/user';
import { DataService } from './../../services/data.service';

@Component({
    selector: 'account-info',
    templateUrl: './account-info.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class AccountInfoComponent {

    public userInfo: Observable<UserInfo>;
    
    constructor(private authService: AuthService, public navCtrl: NavController, private dataService: DataService) {}

    ngOnInit() {
        this.getAccountInfo();
    }

    private getAccountInfo() {
        this.userInfo = this.authService.getAccountInfo();
    }

    public logout() {
        this.navCtrl.setRoot('HomePage');
        this.authService.logout();
        this.dataService.loadChannels();
    }
}
