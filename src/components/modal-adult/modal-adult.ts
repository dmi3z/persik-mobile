import { AuthService } from './../../services/auth.service';
import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'modal-adult',
  templateUrl: './modal-adult.html'
})

export class ModalAdultComponent {

  @Output() pinChecker: EventEmitter<boolean> = new EventEmitter();
  public pin: string;
  public notvalid: boolean = false;

  constructor(private authService: AuthService) {}

  public checkPin(): void {
    this.authService.checkPinIsValid(this.pin).then(res => {
      if (!res) {
        this.notvalid = true;
      } else {
        this.notvalid = false;
        this.pinChecker.emit(res);
      }
    });
  }

  public closeModal(): void {
    this.pinChecker.emit();
  }

}
