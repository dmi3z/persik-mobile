import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { ImageService } from '../../services/image.service';
import { ModalSettings } from '../../models/modal';
import { ContentName } from '../../models/vod';
import { ModalController } from './../../services/modal.controller';
import { AuthService } from './../../services/auth.service';
import { FavoriteService } from './../../services/favorite.service';
import { Observable } from 'rxjs/Observable';
import { DataService } from './../../services/data.service';
import { Tvshow } from './../../models/tvshow';
import { TimeService } from './../../services/time.service';
import { Channel } from './../../models/channel';

@Component({
    selector: 'channel-onair',
    templateUrl: './channel-onair.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ChannelOnairComponent {

    @Input() channel: Channel;

    public logo: string = '';
    public frame: string = '';
    public tvshows: Array<{startTime: string, tvshow: Tvshow}> = [];
    public progress: number = 0;

    private timeSubscriber: Subscription;

    constructor(
        private imageService: ImageService,
        private timeService: TimeService,
        private dataService: DataService,
        private favoriteService: FavoriteService,
        public navCtrl: NavController,
        private authService: AuthService,
        private modalCtrl: ModalController,
        private cdr: ChangeDetectorRef
    ) {}

    ngOnInit(): void {
        this.initChannel();
        this.timeSubscriber = this.timeService.timeController.subscribe(() => {
            this.updateProgressLine();
        });
    }

    public get isFavorite(): Observable<boolean> {
        return this.favoriteService.isChannelFavorite(this.channel.channel_id);
    }

    public getIsTvshowFavorite(id: string): Observable<boolean> {
        return this.favoriteService.isTvshowFavorite(id);
    }

    public openChannel(): void {
        this.navCtrl.push('ChannelPlayerPage', { channel_id: this.channel.channel_id });
    }

    public get isLogin(): boolean {
        return this.authService.isLogin;
    }

    public removeFavoriteChannel(event): void {
        event.stopPropagation();
        this.favoriteService.deleteFromFavorite(new ModalSettings(this.channel.channel_id, ContentName.CHANNEL));
    }

    public removeFavoriteTvshow(event, id: string): void {
        event.stopPropagation();
        this.favoriteService.deleteFromFavorite(new ModalSettings(id, ContentName.TV));
    }

    public isCurrent(index): boolean {
        return index == 0;
    }

    public showModalForChannel(): void {
        this.modalCtrl.create(new ModalSettings(this.channel.channel_id, ContentName.CHANNEL));
    }

    public showModalForTvshow(tvshow: Tvshow): void {
        this.dataService.getVideoInfo(+tvshow.video_id).then(data => {
            if (!data.cover || data.cover.length == 0 ) {
                data.cover = this.imageService.getTvshowFrame(tvshow);
            }
            this.modalCtrl.create(new ModalSettings(tvshow.tvshow_id, ContentName.TV, data));
        });
    }

    public get isAdult(): boolean {
      return this.channel.age_rating == '18+';
    }

    private initChannel(): void {
        this.logo = this.channelLogo;
        this.frame  = this.channelFrame;
        this.getTvshows();
    }

    private updateProgressLine(): void {
        if (this.tvshows[0]) {
            if (this.tvshows[0].tvshow.start && this.tvshows[0].tvshow.stop) {
                this.progress = Math.round(((this.timeService.currentTime - this.tvshows[0].tvshow.start)*100) / (this.tvshows[0].tvshow.stop - this.tvshows[0].tvshow.start));
            } else {
                this.progress = 0;
            }
            if (this.tvshows[0].tvshow.stop <= this.timeService.currentTime) {
                this.tvshows = [];
                this.initChannel();
            }
            this.cdr.detectChanges();
        }
    }

    private get channelFrame(): string {
        return this.imageService.getChannelFrame(this.channel.channel_id, this.timeService.currentTime, 'crop', 256, 144);
    }

    public get channelLogo(): string {
        return this.imageService.getChannelLogo(this.channel.channel_id);
    }

    public get imageStub(): string {
        return this.imageService.getChannelFrame(30, this.timeService.currentTime, 'crop', 256, 144);
    }

    private getTvshows(): void {
        this.dataService.getCurrentTvShow(this.channel.channel_id).then(cur => {
            this.tvshows.push({
                startTime: this.timeService.convertToTime(cur.start),
                tvshow: cur
            });
            this.dataService.getNextTvShow(this.channel.channel_id).then(next => {
                this.tvshows.push({
                    startTime: this.timeService.convertToTime(next.start),
                    tvshow: next
                });
                this.cdr.detectChanges();
            });
        });
    }

    ngOnDestroy() {
        if (this.timeSubscriber) this.timeSubscriber.unsubscribe();
    }

}
