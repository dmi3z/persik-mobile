import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'progress-line',
    templateUrl: './progress-line.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ProgressLineComponent {

    @Input() progress: number = 0; // %

    constructor() {}

}