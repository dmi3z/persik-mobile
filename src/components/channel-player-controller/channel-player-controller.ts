import { Subscription } from 'rxjs/Subscription';
import { OrientationService } from './../../services/orientation.service';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Channel } from './../../models/channel';
import { Tvshow } from './../../models/tvshow';
import { TimeService } from './../../services/time.service';
import scrollIntoView from 'scroll-into-view-if-needed';
import { VideoControlEventInfo, ControlEvents } from '../../models/player';

@Component({
    selector: 'channel-player-controller',
    templateUrl: './channel-player-controller.html'
})

export class ChannelPlayerControllerComponent {

    @Input() selectedChannel: Channel;
    @Input() channels : Channel[];
    @Input() tvshows: Tvshow[];
    @Input() isPlaying: boolean;
    @Input() isCanPlay: boolean;

    @Output() onChannelChange: EventEmitter<Channel> = new EventEmitter<Channel>();
    @Output() controlEvents: EventEmitter<VideoControlEventInfo> = new EventEmitter<VideoControlEventInfo>();

    public isShowControls: boolean = true;

    private controlsTimer: any;
    private showControlsTimeout: number = 4000; // время отображение контролов в мс.
    private orientationSubscriber: Subscription;

    constructor(
        private timeService: TimeService,
        private screenOrientation: ScreenOrientation,
        private orientationService: OrientationService
    ) {}

    ngOnInit() {
        if (!this.selectedChannel) this.selectedChannel = this.channels[0];

        this.orientationSubscriber = this.orientationService.onChange.subscribe(() => {
            this.screenOrientation.unlock();
        });
    }

    public showControls(): void {
        this.isShowControls = true;
        clearTimeout(this.controlsTimer);
        this.controlsTimer = setTimeout(() => {
            this.isShowControls = false;
        }, this.showControlsTimeout);
    }

    ngAfterViewInit() {
        this.showControls();
        this.scrollToCurrentChannel();
        this.scrollToCurrentTvshow();
    }

    public resume(): void {
        this.controlEvents.next(new VideoControlEventInfo(ControlEvents.CONTROL_PLAY));
    }

    public pause(): void {
        this.controlEvents.next(new VideoControlEventInfo(ControlEvents.CONTROL_PAUSE));
    }

    public get isLandscape(): boolean {
        return this.screenOrientation.type.includes('landscape');
    }

    public isActiveChannel(channel: Channel): boolean {
        return channel.channel_id == this.selectedChannel.channel_id;
    }

    public isCurrentTvshow(tvshow: Tvshow): boolean {
        const currentTime: number = this.timeService.currentTime;
        return tvshow.start <= currentTime && tvshow.stop > currentTime;
    }

    public changeActiveChannel(channel: Channel): void {
        if (this.selectedChannel.channel_id != channel.channel_id) {
            this.tvshows = null;
            this.onChannelChange.next(channel);
        }
        this.scrollToCurrentChannel();
        this.scrollToCurrentTvshow();
    }

    public toggleOrientation(): void {
        if (this.isLandscape) {
            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        } else {
            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
        }
    }

    private scrollToCurrentChannel(): void {
        setTimeout(() => {
            const channel = document.getElementsByClassName('channel-card_active')[0];
            if (channel)
                scrollIntoView(channel, {
                    behavior: 'smooth',
                    scrollMode: 'always',
                    block: 'center',
                    inline: 'center'
                });
        }, 0);
    }

    private scrollToCurrentTvshow(): void {
        setTimeout(() => {
            const tvshow = document.getElementsByClassName('tape-item-active')[0];
            if (tvshow)
                scrollIntoView(tvshow, {
                    behavior: 'smooth',
                    scrollMode: 'always',
                    block: 'center',
                    inline: 'center'
                });
        }, 1000);
    }

    ngOnDestroy() {
        if (this.orientationSubscriber) this.orientationSubscriber.unsubscribe();
    }

}
