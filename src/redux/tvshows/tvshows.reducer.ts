import { Tvshow } from './../../models/tvshow';
import { TVSHOWS_ACTION, TvshowAction } from './tvshows.action';

export interface State {
    tvshows: Tvshow[]
}

const initialState: State = {
    tvshows: []
}

export function tvshowsReducer(state = initialState, action: TvshowAction) {
    switch (action.type) {
        case TVSHOWS_ACTION.LOAD_TVSHOWS:
            return {
                tvshows: [...state.tvshows, ...action.tvshows]
            }
        default:
            return state;
    }
}
[]