import { Tvshow } from './../../models/tvshow';

export interface TvshowsState {
    tvshowsData: {
        tvshows: Tvshow[];
    }
    
}