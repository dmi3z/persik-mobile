import { Action } from '@ngrx/store';
import { Tvshow } from '../../models/tvshow';

export namespace TVSHOWS_ACTION {
    export const LOAD_TVSHOWS = 'LOAD_TVSHOWS';
}

export class LoadTvshows implements Action {
    readonly type = TVSHOWS_ACTION.LOAD_TVSHOWS;
    
    constructor(public tvshows: Tvshow[]){}
}

export type TvshowAction = LoadTvshows;