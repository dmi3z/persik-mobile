import { VOD_ACTION, VodAction } from './vod.action';
import { Category } from '../../models/category';
import { VodSections } from '../../models/vod';

export interface State {
    categories: Category[];
    featured: VodSections
}

const initialState: State = {
    categories: [],
    featured: {
        sections: []
    }
}

export function vodReducer(state = initialState, action: VodAction) {
    switch (action.type) {
        case VOD_ACTION.LOAD_CATEGORIES:
            return {
                categories: [...state.categories, ...action.categories],
                featured: state.featured
            }
        case VOD_ACTION.LOAD_HOME_VOD:
            return {
                categories: state.categories,
                featured: {
                    sections: [...state.featured.sections, ...action.featured.sections]
                }
            }
        default:
            return state;
    }
}
[]