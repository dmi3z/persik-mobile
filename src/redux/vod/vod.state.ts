import { VodCategories } from '../../models/vod';

export interface VodState {
    vodCategories: VodCategories
}