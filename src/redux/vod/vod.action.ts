import { Action } from '@ngrx/store';
import { Category } from '../../models/category';
import { VodSections } from '../../models/vod';

export namespace VOD_ACTION {
    export const LOAD_CATEGORIES = 'LOAD_CATEGORIES';
    export const LOAD_HOME_VOD = 'LOAD_HOME_VOD';
}

export class LoadCategories implements Action {
    readonly type = VOD_ACTION.LOAD_CATEGORIES;
    
    constructor(public categories: Category[]){}
}

export class LoadHomeVod implements Action {
    readonly type = VOD_ACTION.LOAD_HOME_VOD;
    
    constructor(public featured: VodSections){}
}

export type VodAction = LoadCategories | LoadHomeVod;
