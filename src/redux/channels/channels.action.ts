import { Action } from '@ngrx/store';
import { Channel, FeatureChannel } from '../../models/channel';
import { Genre } from '../../models/category';

export namespace CHANNELS_ACTION {
    export const LOAD_CHANNELS = 'LOAD_CHANNELS';
    export const LOAD_FEATURED_CHANNELS = 'LOAD_FEATURED_CHANNELS';
    export const LOAD_CHANNEL_CATEGORIES = 'LOAD_CHANNEL_CATEGORIES';
}

export class LoadChannels implements Action {
    readonly type = CHANNELS_ACTION.LOAD_CHANNELS;
    
    constructor(public channels: Channel[]){}
}

export class LoadFeaturedChannels implements Action {
    readonly type = CHANNELS_ACTION.LOAD_FEATURED_CHANNELS;

    constructor(public featured_channels: FeatureChannel[]){}
}

export class LoadChannelCategories implements Action{
    readonly type = CHANNELS_ACTION.LOAD_CHANNEL_CATEGORIES;

    constructor(public categories: Genre[]) {}
}

export type ChannelsAction = LoadChannels | LoadFeaturedChannels | LoadChannelCategories;