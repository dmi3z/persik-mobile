import { Channel } from "../../models";
import { FeatureChannel } from "../../models/channel";
import { Genre } from "../../models/category";

export interface ChannelsState {
    channelsData: {
        channels: Channel[],
        featured_channels: FeatureChannel[],
        categories: Genre[]
    }
}