import { Channel, FeatureChannel } from '../../models/channel';
import { CHANNELS_ACTION, ChannelsAction } from './channels.action';
import { Genre } from '../../models/category';

export interface State {
    channels: Channel[];
    featured_channels: FeatureChannel[];
    categories: Genre[];
}

const initialState: State = {
    channels: [],
    featured_channels: [],
    categories: []
}

export function channelsReducer(state = initialState, action: ChannelsAction) {
    switch (action.type) {
        case CHANNELS_ACTION.LOAD_CHANNELS:
            return {
                ...state,
                channels: [...action.channels]
            }
        case CHANNELS_ACTION.LOAD_FEATURED_CHANNELS:
            return {
                ...state,
                featured_channels: [...action.featured_channels]
            }
        case CHANNELS_ACTION.LOAD_CHANNEL_CATEGORIES:
            return {
                ...state,
                categories: [...action.categories]
            }
        default:
            return state;
    }
}
[]