import { FavoritesData } from "../../models/favorite";

export interface FavoritesState {
    favoritesData: FavoritesData;
}