import { FavoriteChannel, FavoriteTvshow, FavoriteVideo } from "../../models/favorite";
import { FavoriteAction, FAVORIE_ACTION } from "./favorite.action";

export interface State {
    channels: FavoriteChannel[];
    tvshows: FavoriteTvshow[];
    videos: FavoriteVideo[];
}

const initialState: State = {
    channels: [],
    tvshows: [],
    videos: []
}

export function favoritesReducer(state = initialState, action: FavoriteAction) {
    switch (action.type) {
        case FAVORIE_ACTION.LOAD_FAVORITE_CHANNELS:
            return {
                ...state,
                channels: [...state.channels, ...action.channels]
            }
        case FAVORIE_ACTION.LOAD_FAVORITE_TVSHOWS:
            return {
                ...state,
                tvshows: [...state.tvshows, ...action.tvshows]
            }
        case FAVORIE_ACTION.LOAD_FAVORITE_VIDEOS:
            return {
                ...state,
                videos: [...state.videos, ...action.videos]
            }
        case FAVORIE_ACTION.DELETE_FAVORITE_CHANNEL:
            return {
                ...state,
                channels: [...state.channels.filter(ch => ch.channel_id != action.id)]
            }
        case FAVORIE_ACTION.ADD_FAVORITE_CHANNEL:
            return {
                ...state,
                channels: [...state.channels, action.channel]
            }
        case FAVORIE_ACTION.DELETE_FAVORITE_TVSHOW:
            return {
                ...state,
                tvshows: [...state.tvshows.filter(tv => tv.tvshow_id != action.id)]
            }
        case FAVORIE_ACTION.ADD_FAVORITE_TVSHOW:
            return {
                ...state,
                tvshows: [...state.tvshows, action.tvshow]
            }
        case FAVORIE_ACTION.DELETE_FAVORITE_VIDEO:
            return {
                ...state,
                videos: [...state.videos.filter(video => video.video_id !== action.id)]
            }
        case FAVORIE_ACTION.ADD_FAVORITE_VIDEO:
            return {
                ...state,
                videos: [...state.videos, action.video]
            }
        default:
            return state;
    }
}
[]