import { Genre } from './category';

export class Channel {
    channel_id: number;
    name: string;
    dvr_sec: number;
    age_rating: string;
    genres: number[];
    logo: string;
    priority: number;
    rank: number;
    available: boolean;
    stream_url: string;
}

export class FeatureChannel {
    channel_id: number;
}

export interface Channels {
    channels: Channel[],
    featured_channels: FeatureChannel[],
    categories: Genre[]
}

export interface FeaturedChannelsTape {
  genre_id: number;
  title: string;
  channels: Channel[];
}
