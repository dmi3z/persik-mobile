export interface UserInfo {
    about: string;
    email: string;
    name: string;
    pass_code: string;
    sex: string;
    user_id: string;
    year: string;
}

export interface UserSubscription {
    created_at: string;
    product_id: number;
    product_name: string;
    product_option_name: string;
    started_at: string;
    expired_at: string;
    cost: number;
    is_selected?: boolean;
}

export interface TariffOption {
    product_option_id: number;
    name: string;
    price: number;
    currency: string;
    price_formatted: string;
}

export interface Tariff {
    product_id: number;
    name: string;
    options: TariffOption[];
    is_selected?: boolean;
}

export class Products {
    currency: string;
    pay_sys: string[];
    products: Tariff[];

    constructor() {
        this.products = [];
    }
}

export interface authUser {
    auth_token: string;
    user_id: string;
}

export class Payment {
    pay_sys: string;
    product_option_id: number[];

    constructor(pay_sys: string, option_id: number[]) {
        this.pay_sys = pay_sys;
        this.product_option_id = option_id;
    }
}

export interface PaymentInfo {
    description: string;
    invoice_id: number;
    payment_page_url: string;
    sms: Sms
    user_id: string;
}

interface Sms {
    phone: string;
    message: string;
}
