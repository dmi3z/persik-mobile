import { Video, ContentType } from './vod';

export class ModalSettings {
    id: number | string;
    type: ContentType;
    info?: Video;
    genres?: string[];

    constructor(id: number | string, type: ContentType, info?: Video, genres?: string[]) {
        this.id = id;
        this.type = type;
        this.info = info;
        this.genres = genres;
    }
}

 /* export interface ModalButton {
    id: number;
    name: string;
    action: 'close' | 'add_to_favorite' | 'remove_from_favorite' | 'show_info' | 'play';
}

export namespace BUTTON_ACTIONS {
    export const CLOSE = 'close';
    export const ADD_TO_FAVORITE = 'add_to_favorite';
    export const REMOVE_FROM_FAVORITE = 'remove_from_favorite';
    export const SHOW_INFO = 'show_info';
    export const PLAY = 'play';
} */