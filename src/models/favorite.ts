export interface FavoriteChannel {
    channel_id: number;
    added_time: number;
}

export interface FavoriteTvshow {
    tvshow_id: string;
    added_time: number;
}

export interface FavoriteVideo {
    video_id: number;
    added_time: number;
}

export interface FavoritesData {
    channels: FavoriteChannel[];
    videos: FavoriteVideo[];
    tvshows: FavoriteTvshow[];
}
