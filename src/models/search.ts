export interface SearchData {
    channels: Array<{channel_id: number}>,
    videos: Array<{video_id: number}>,
    tvshows: Array<{tvshow_id: string}>
}