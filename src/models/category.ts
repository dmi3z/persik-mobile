export class Genre {
    id: number;
    name: string;
    name_en: string;
    is_main: boolean;
}

export class Category {
    id: number;
    name: string;
    name_en: string;
    genres: Genre[];
}