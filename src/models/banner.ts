export class Banner {
    banner_id: number;
    name: string;
    element_type: string;
    element_id: number;
    url: string;
    utm: string;
    img_url_desktop: string;
    img_url_mobile: string;
    img_url_tv: string;

    constructor() {
        this.banner_id = 0;
        this.name = '';
        this.element_type = '';
        this.element_id = 0;
        this.url = '';
        this.utm = 'app-mobile';
        this.img_url_desktop = '';
        this.img_url_mobile = '';
        this.img_url_tv = '';
    }
}