import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import 'rxjs/add/operator/map';

import { Channel, Category, Tvshow, Genre, Banner, FeatureChannel } from './../models/';
import { LoadCategories, LoadHomeVod } from './../redux/vod/vod.action';
import { VodState } from './../redux/vod/vod.state';
import { TimeService } from './time.service';
import { VodSections, VodSource, VideoInterface, Video, Country, RatingFilter } from '../models/vod';
import { LoadChannels, LoadFeaturedChannels, LoadChannelCategories } from '../redux/channels/channels.action';
import { ChannelStreamData, VideoStreamData } from '../models/stream';
import { SearchData } from './../models/search';
import { ChannelsState } from '../redux/channels/channels.state';
import { FeaturedChannelsTape } from '../models/channel';


@Injectable()
export class DataService {

  private BASE_URL: string = 'https://api.persik.by/';

  constructor(private http: HttpClient, private channelsStore: Store<ChannelsState>, private timeService: TimeService, private vodStore: Store<VodState>) { }

  loadChannels(): void {
    this.http.get<channelsArray>(this.BASE_URL.concat('v2/content/channels')).toPromise().then(data => {
      this.channelsStore.dispatch(new LoadChannels(data.channels));
    })
  }

  loadFeatureChannels(): void {
    this.http.get<featuredChannels>(this.BASE_URL.concat('v2/featured/channels?size=10'))
      .toPromise().then(featured_channels => {
        this.channelsStore.dispatch(new LoadFeaturedChannels(featured_channels.channels));
      });
  }

  getCurrentTvShow(channelId: number): Promise<Tvshow> {
    const date: string = this.timeService.getDate();
    const params: HttpParams = new HttpParams().set('channels[]', channelId.toString()).set('from', date).set('to', date).set('limit', '500');
    const time: number = this.timeService.currentTime;
    return this.http.get<tvshowsInterface>(this.BASE_URL.concat('v2/epg/tvshows'), { params })
      .map(response => {
        const data: Tvshow[] = response.tvshows.items;
        const tvshow = data.find(tvshow => {
          return tvshow.start <= time && tvshow.stop >= time;
        });
        if (tvshow) return tvshow;
        return new Tvshow();
      }).toPromise();
  }

  getNextTvShow(channel_id: number): Promise<Tvshow> {
    const date: string = this.timeService.getDate();
    const params: HttpParams = new HttpParams().set('channels[]', channel_id.toString()).set('from', date).set('to', date).set('limit', '500');
    const time: number = this.timeService.currentTime;
    return this.http.get<tvshowsInterface>(this.BASE_URL.concat('v2/epg/tvshows'), { params })
      .map(response => {
        const data: Tvshow[] = response.tvshows.items;
        const tvshow = data.find(tvshow => {
          return tvshow.stop >= time && tvshow.start > time;
        });
        if (tvshow) return tvshow;
        return new Tvshow();
      }).toPromise();
  }

  getTvshows(channel_id: number, date: string): Promise<Tvshow[]> {
    const params: HttpParams = new HttpParams().set('channels[]', channel_id.toString()).set('from', date).set('to', date);
    return this.http.get<tvshowsInterface>(this.BASE_URL.concat('v2/epg/tvshows'), { params }).map(response => {
      return response.tvshows.items;
    }).toPromise();
  }

  loadChannelCategories(): void {
    this.http.get(this.BASE_URL.concat('v2/categories/channel'))
      .toPromise().then((categories: Genre[]) => {
        categories.unshift({
          id: 0,
          name: 'Все жанры',
          name_en: 'All genres',
          is_main: true
        });
        this.channelsStore.dispatch(new LoadChannelCategories(categories));
      });
  }

  loadVodCategories(): void {
    this.http.get(this.BASE_URL.concat('v2/categories/video'))
      .toPromise().then((categories: Category[]) => {
        this.vodStore.dispatch(new LoadCategories(categories));
      });
  }

  loadVodFeatured() {
    return this.http.get<VodSections>(this.BASE_URL.concat('v2/featured')).toPromise().then(featured => {
      featured.sections.forEach(section => {
        this.loadVodHomeContent(section.source).subscribe(res => {
          Object.assign(section, res);
        });
      });
      this.vodStore.dispatch(new LoadHomeVod(featured));
    });
  }

  private loadVodHomeContent(source: VodSource): Observable<VideoInterface> {
    const params: HttpParams = new HttpParams()
      .set('size', source.params.size.toString())
      .set('category_id', source.params.category_id ? source.params.category_id.toString() : '')
      .set('genre_id', source.params.genre_id ? source.params.genre_id.toString() : '');
    return this.http.get<VideoInterface>(this.BASE_URL.concat(`v${source.version}/${source.module}/${source.action}`), { params });
  }

  getVideoInfo(id: number): Promise<Video> {
    const params: HttpParams = new HttpParams()
      .set('id[]', id.toString());
    return this.http.get<VideoInformationBackend>(this.BASE_URL.concat('v2/content/video'), { params }).map(res => {
      return res.videos[0];
    }).toPromise();
  }

  getTvshowInfo(id: string): Promise<Tvshow> {
    const params: HttpParams = new HttpParams()
      .set('id[]', id);
    return this.http.get<TvshowInformationBackend>(this.BASE_URL.concat('v2/content/tvshow'), { params }).map(res => res.tvshows[0]).toPromise();

  }

  loadVideoContent(category_id: number, genre_id: number, size?: number,
    skip?: number, country?: Country, sort?: RatingFilter): Promise<VideoInterface> {
    let type = 'sort';
    if (sort && sort.value.split(' ').length === 2) {
      type = 'year';
    }
    const params: HttpParams = new HttpParams()
      .set('size', (size && size !== 0) ? size.toString() : '')
      .set('offset', (skip && skip !== 0) ? skip.toString() : '')
      .set('category_id', category_id.toString())
      .set('genre_id', (genre_id !== 0 && genre_id) ? genre_id.toString() : '')
      .set('country_id', (country && country.id !== 0) ? country.id.toString() : '')
      .set('sort', (type === 'sort' && sort && sort.value) ? sort.value : 'last')
      .set('year_from', (type === 'year') ? sort.value.split(' ')[0] : '')
      .set('year_to', (type === 'year') ? sort.value.split(' ')[1] : '');
    return this.http.get<VideoInterface>(this.BASE_URL.concat('v2/content/videos'), { params }).toPromise();
  }

  getBanners(): Promise<Banner[]> {
    return this.http.get<Banner[]>(this.BASE_URL.concat('v2/content/banners2')).map(res => {
      return res.filter(banner => banner.img_url_mobile.length > 0)
    }).toPromise();
  }

  searchContent(text: string): Promise<SearchData> {
    const params: HttpParams = new HttpParams()
      .set('query', text)
      .set('channels', 'true')
      .set('videos', 'true')
      .set('tvshows', 'true');
    return this.http.get<SearchData>(this.BASE_URL.concat('v2/search'), { params }).toPromise();
  }

  getChannelStream(id: number): Promise<ChannelStreamData> {
    const params: HttpParams = new HttpParams().set('id', id.toString());
    return this.http.get<ChannelStreamData>(this.BASE_URL.concat('v1/stream/channel'), { params }).toPromise();
  }

  getVideoStream(id: number): Promise<VideoStreamData> {
    const params: HttpParams = new HttpParams().set('id', id.toString());
    return this.http.get<VideoStreamData>(this.BASE_URL.concat('v1/stream/video'), { params }).toPromise();
  }

  getTvshowStream(id: string): Promise<ChannelStreamData> {
    const params: HttpParams = new HttpParams().set('id', id);
    return this.http.get<ChannelStreamData>(this.BASE_URL.concat('v1/stream/show'), { params }).toPromise();
  }

  getChannelsForFeatured(genres: Genre[]): Promise<FeaturedChannelsTape[]> {
    return new Promise(resolve => {
      if (genres.length > 0) {
        const fChs: FeaturedChannelsTape[] = [];
        this.channelsStore.select('channelsData').subscribe(chs => {
          for (let i = 0; i < genres.length; i++) {
            const channels = chs.channels.filter(ch => ch.genres.some(g => +g === +genres[i].id));
            if (channels && channels.length > 0) {
              fChs.push({
                genre_id: genres[i].id,
                title: genres[i].name,
                channels
              });
            }
          }
          resolve(fChs);
        })
      }
    });
  }
}

interface VideoInformationBackend {
  videos: Video[];
}

interface TvshowInformationBackend {
  tvshows: Tvshow[];
}


interface channelsArray {
  channels: Channel[]
}

interface featuredChannels {
  channels: FeatureChannel[]
}

interface tvshowsInterface {
  tvshows: {
    items: Tvshow[]
  }
}
