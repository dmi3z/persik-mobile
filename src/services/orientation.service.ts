import { Injectable, EventEmitter } from '@angular/core';
import { DeviceMotion, DeviceMotionAccelerationData, DeviceMotionAccelerometerOptions } from '@ionic-native/device-motion';

@Injectable()

export class OrientationService {

    public onChange: EventEmitter<string> = new EventEmitter();
    
    private orientation: EventEmitter<string> = new EventEmitter();
    private delay: number = 500;                    // Время задержки перед эмитом смены ориентации
    private criticalLevel: number = 5;              // Значение датчика по Х после которого становится альбомным
    private detectTimer: any;
    private previousValue: string;

    constructor(private deviceMotion: DeviceMotion) {
        
        this.startWatch();

        this.orientation.subscribe(orientation => {
            if (orientation != this.previousValue) {
                clearTimeout(this.detectTimer);
                this.detectTimer = setTimeout(() => {
                    this.onOrientationChange(orientation);
                }, this.delay);
            }
            this.previousValue = orientation;
        });
    }

    public startWatch(): void {
        const options: DeviceMotionAccelerometerOptions = {
            frequency: 100
        };
        this.deviceMotion.watchAcceleration(options).subscribe((data: DeviceMotionAccelerationData) => {
            const orientation: string = this.getOrientation(data.x);
            this.orientation.next(orientation);            
        }, err => {
            console.log('Cordova not available');
        });
    }

    private onOrientationChange(orientation: string): void {
        this.onChange.next(orientation);
    }
    
    private getOrientation(x: number): string {
        if (Math.abs(x) > this.criticalLevel) return 'landscape';
        return 'portrait';
    }

}
