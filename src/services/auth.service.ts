import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { UserInfo, UserSubscription, Products, authUser, Payment, PaymentInfo } from '../models/user';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';


@Injectable()
export class AuthService {

    private BASE_URL: string = 'https://api.persik.by/';
    private loginState: EventEmitter<boolean> = new EventEmitter<boolean>(true);
    public uuid: any = '12345';

    constructor(private http: HttpClient, private uuidService: UniqueDeviceID) {}

    checkEmail(email: string): Promise<emailCheck> {
        const params:HttpParams = new HttpParams().set('email', email);
        return this.http.get<emailCheck>(this.BASE_URL.concat('v2/auth/check'), { params }).toPromise();
    }

    login(email: string, password: string): Promise<authUser> {
        const params: HttpParams = new HttpParams().set('email', email).set('password', password);
        return this.http.post<authUser>(this.BASE_URL.concat('v1/account/login'), {}, { params }).toPromise();
    }

    register(email: string, password: string): Promise<any> {
        const params: HttpParams = new HttpParams().set('email', email).set('password', password);
        return this.http.post<authUser>(this.BASE_URL.concat('v1/account/registration'), {}, { params }).toPromise();
    }

    logout() {
        this.token = '';
        this.user_id = '';
        this.loginState.next();
    }

    changeLoginState() {
        this.loginState.next();
    }

    getAccountInfo(): Observable<UserInfo> {
        return this.http.get<UserInfo>(this.BASE_URL.concat('v1/account/info'));
    }

    getUserSubscriptions(): Promise<UserSubscription[]> {
        return this.http.get<UserSubscription[]>(this.BASE_URL.concat('v2/billing/subscriptions')).toPromise();
    }

    getTariffs(): Promise<Products> {
      return this.http.get<Products>(this.BASE_URL.concat('v2/billing/products')).toPromise();
    }

    getAllTariffs(): Promise<Products> {
      return this.http.get<Products>(this.BASE_URL.concat('v2/billing/productsActive')).toPromise();
    }

    createPayment(payment: Payment): Promise<PaymentInfo> {
        const formData: FormData = new FormData();
        formData.append('pay_sys', payment.pay_sys);
        formData.append('product_option_id[]', `${payment.product_option_id.toString()}`);
        return this.http.post<PaymentInfo>(this.BASE_URL.concat('v2/billing/payment'), formData).toPromise();
    }

    checkPinIsValid(pin: string): Promise<boolean> {
      return this.getAccountInfo().map(res => {
        return res.pass_code == pin;
      }).toPromise();
    }

    get loginStateController(): Observable<any> {
        return this.loginState.asObservable();
    }

    get isLogin(): boolean {
        if (this.token.length > 0 && this.user_id.length > 0) return true;
        return false;
    }

    set token(token: string) {
        if (token.length > 0) {
            localStorage.setItem('user_token', token);
        } else {
            localStorage.removeItem('user_token');
        }
    }

    get token(): string {
        const token = localStorage.getItem('user_token');
        if (token) return token;
        return '';
    }

    set user_id(user_id: string) {
        if (user_id.length > 0) {
            localStorage.setItem('user_id', user_id);
        } else {
            localStorage.removeItem('user_id');
        }
    }

    get user_id(): string {
        const id = localStorage.getItem('user_id');
        if (id) return id;
        return '';
    }

    loadUuid(): void {
        this.uuidService.get().then(res => this.uuid = res).catch(() => {
            console.log('Cordova not available');
        });
    }
}

interface emailCheck {
    name?: string;
    exists: boolean;
}
