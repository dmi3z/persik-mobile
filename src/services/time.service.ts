import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class TimeService {

    private interval: number = 5000;
    private fastInterval: number = 1000;
    public timeController: Subject<number> = new Subject<number>();
    public timeControllerFast: Subject<number> = new Subject<number>();

    constructor() {
        setInterval(()=> {
            this.timeController.next(this.currentTime);
        }, this.interval);

        setInterval(()=> {
            this.timeControllerFast.next(this.currentTime);
        }, this.fastInterval);
    }

    get currentTime(): number {
        return Math.round(new Date().getTime() / 1000);
    }

    getDate():string {
        const date:Date = new Date();
        const year:number = date.getFullYear();
        const month:number = date.getMonth() + 1;
        const day:number = date.getDate();
        return `${year}-${month}-${day}`;
    }

    convertToTime(time: number): string {
        if (time) {
            const unixTime = new Date(time * 1000);
            return `${unixTime.getHours()}:${unixTime.getMinutes() >= 10 ? unixTime.getMinutes() : '0'+unixTime.getMinutes().toString()}`;
        }
        return '';        
    }

    convertToDate(date: string): string { // from 2017-09-28 11:30:55 to 28.09.2017
        if (date.length > 10) {
            const dsa: string[] = date.split(' ')[0].split('-');
            return `${dsa[2]}.${dsa[1]}.${dsa[0]}`;
        } return '';        
    }

    getDurationFromTime(time: number): string {
        if (time) {
            const hours: number = Math.floor(time / 3600);
            const minutes: number = Math.floor((time - hours * 3600) / 60);
            const seconds: number = Math.floor(time - hours * 3600 - minutes * 60);
            return `${this.checkZero(hours)}:${this.checkZero(minutes)}:${this.checkZero(seconds)}`;
        }
        return '';
    }

    private checkZero(time: number) {
        return `${time > 9 ? time : '0'+time}`;
    }
}