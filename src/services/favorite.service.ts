import { ToastController } from 'ionic-angular';
import { AddFavoriteChannel, DeleteFavoriteTvshow, AddFavoriteTvshow, DeleteFavoriteVideo, AddFavoriteVideo } from './../redux/favorite/favorite.action';
import { ModalSettings } from './../models/modal';
import { Store } from '@ngrx/store';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { FavoriteChannel, FavoriteVideo, FavoriteTvshow } from '../models/favorite';
import { FavoritesState } from '../redux/favorite/favorite.state';
import { LoadFavoriteChannels, LoadFavoriteVideos, LoadFavoriteTvshows, DeleteFavoriteChannel } from '../redux/favorite/favorite.action';
import { TimeService } from './time.service';


@Injectable()
export class FavoriteService {

    private BASE_URL: string = 'https://api.persik.by/';

    constructor(private http: HttpClient, private favoriteStore: Store<FavoritesState>, private timeService: TimeService, private toastCtrl: ToastController) {}

    loadFavorite(): void {
        this.http.get<FavoriteChannels>(this.BASE_URL.concat('v2/favorite/channels')).subscribe(data => {
            this.favoriteStore.dispatch(new LoadFavoriteChannels(data.channels));
        });
        this.http.get<FavoriteVideos>(this.BASE_URL.concat('v2/favorite/videos')).subscribe(data => {
            this.favoriteStore.dispatch(new LoadFavoriteVideos(data.videos));
        });
        this.http.get<FavoriteTvshows>(this.BASE_URL.concat('v2/favorite/tvshows')).subscribe(data => {
            this.favoriteStore.dispatch(new LoadFavoriteTvshows(data.tvshows));
        });
    }

    isChannelFavorite(id: number): Observable<boolean> {
        return this.favoriteStore.select('favoritesData').map(data => {
            return data.channels.some(channel => channel.channel_id == id);
        });
    }

    isTvshowFavorite(id: string): Observable<boolean> {
        return this.favoriteStore.select('favoritesData').map(data => {
            return data.tvshows.some(tvshow => tvshow.tvshow_id.toString() == id);
        });
    }

    isVideoFavorite(id: number): Observable<boolean> {
        return this.favoriteStore.select('favoritesData').map(data => {
            return data.videos.some(video => video.video_id == id);
        });
    }

    deleteFromFavorite(settings: ModalSettings) {
        const params:HttpParams = new HttpParams().set('id', settings.id.toString());
        this.http.delete(this.BASE_URL.concat('v2/favorite/', settings.type != 'tv' ? settings.type : 'tvshow'), { params }).subscribe(() => {
            if (settings.type == 'channel') {
                this.favoriteStore.dispatch(new DeleteFavoriteChannel(+settings.id));
                let toast = this.toastCtrl.create({
                    message: "Канал удален из избранного",
                    duration: 3000,
                    position: 'top'
                  });
                toast.present();
            }
            if (settings.type == 'tv') {
                this.favoriteStore.dispatch(new DeleteFavoriteTvshow(settings.id.toString()));
                let toast = this.toastCtrl.create({
                    message: "Передача удалена из избранного",
                    duration: 3000,
                    position: 'top'
                  });
                toast.present();
            }
            if (settings.type == 'video') {
                this.favoriteStore.dispatch(new DeleteFavoriteVideo(+settings.id));
                let toast = this.toastCtrl.create({
                    message: "Видео удалено из избранного",
                    duration: 3000,
                    position: 'top'
                  });
                toast.present();
            }
        });
    }

    addToFavorite(settings: ModalSettings) {
        const params:HttpParams = new HttpParams().set('id', settings.id.toString());
        this.http.post(this.BASE_URL.concat('v2/favorite/', settings.type != 'tv' ? settings.type : 'tvshow'), {}, { params }).subscribe(() => {
            if (settings.type == 'channel') {
                const ch: FavoriteChannel = {
                    channel_id: +settings.id,
                    added_time: this.timeService.currentTime
                }
                this.favoriteStore.dispatch(new AddFavoriteChannel(ch));
                let toast = this.toastCtrl.create({
                    message: "Канал добавлен в избранное",
                    duration: 3000,
                    position: 'top'
                  });
                toast.present();
            };
            if (settings.type == 'tv') {
                const tv: FavoriteTvshow = {
                    tvshow_id: settings.id.toString(),
                    added_time: this.timeService.currentTime
                }
                this.favoriteStore.dispatch(new AddFavoriteTvshow(tv));
                let toast = this.toastCtrl.create({
                    message: "Передача добавлена в избранное",
                    duration: 3000,
                    position: 'top'
                  });
                toast.present();
            };
            if (settings.type == 'video') {
                const video: FavoriteVideo = {
                    video_id: +settings.id,
                    added_time: this.timeService.currentTime
                }
                this.favoriteStore.dispatch(new AddFavoriteVideo(video));
                let toast = this.toastCtrl.create({
                    message: "Видео добавлено в избранное",
                    duration: 3000,
                    position: 'top'
                  });
                toast.present();
            };
        });
    }

}

interface FavoriteChannels {
    channels: FavoriteChannel[];
}

interface FavoriteVideos {
    videos: FavoriteVideo[];
}

interface FavoriteTvshows {
    tvshows: FavoriteTvshow[];
}
