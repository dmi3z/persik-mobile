import { DataService } from './data.service';
import { TimeService } from './time.service';
import { Tvshow } from './../models/tvshow';
import { Injectable } from '@angular/core';


@Injectable()
export class ImageService {

    constructor(private timeService: TimeService, private dataService: DataService) {}

    getChannelFrame(id: number, time: number, transform: string = 'crop', width: number = 288, height: number = 168):string {
        const t = Math.round(time);
        return `https://persik.by/utils/show-frame.php?c=${id}&t=${t}&tr=${transform}&w=${width}&h=${height}`;
    }

    getChannelLogo(id: number) {
        return `https://persik.by/media/channels/logos/${id}.png`;
    }

    /* loadTvshowThumbnail(tvshow: Tvshow): Promise<string> {
        return new Promise((resolve) => {
            if (this.isFuture(tvshow) && tvshow.cover && tvshow.cover.length != 0) {
                resolve();
            }    
            if (this.isFuture(tvshow) && (!tvshow.cover || tvshow.cover.length == 0)) {
                
            }    
            if (this.isCurrent) {
                observer(this.imageService.getChannelFrame(this.tvshow.channel_id, this.timeService.currentTime, 'crop', 256, 144));
            }    
            observer(this.imageService.getTvshowFrame(this.tvshow));
        });        
    } */

    loadTvshowThumbnail(tvshow: Tvshow) {  // Самый кошмарный метод во всем приложении
        return new Promise(resolve => {
                this.getCoverFromVideo(tvshow)
                    .then(res => {                                      //    - Пытаемся загрузить обложку из видео
                        Object.assign(tvshow, res);
                        resolve();
                    })
                    .catch(() => {
                        if (tvshow.cover && tvshow.cover.length > 0) { // -Если в видео обложки нет, но есть в самом твшоу то заканчиваем
                            resolve();
                        } else {                                       // - Если не заканчием
                            if (this.isFuture(tvshow)) {               //       если из будущего
                                tvshow.cover = null;
                                resolve();
                            } else {                                   //       если не из будущего
                                tvshow.cover = this.getTvshowFrame(tvshow);
                                if (tvshow.cover && tvshow.cover.length > 0) {
                                    resolve();
                                } else {
                                    tvshow.cover = null;
                                    resolve();
                                }
                                
                            }                            
                        }                        
                    });
        });
    }

    private getCoverFromVideo(tvshow: Tvshow): Promise<{cover: string}> {
        return new Promise((resolve, reject) => {
            this.dataService.getVideoInfo(+tvshow.video_id).then(res => {
                if (res && res.cover && res.cover.length > 0) {
                    resolve({ cover: res.cover });
                } else {
                    reject(); 
                }                                          
            });
        });
    }

    private isFuture(tvshow: Tvshow): boolean {
        return tvshow.start > this.timeService.currentTime;
    }

    getTvshowFrame(tvshow: Tvshow): string {
        const currentTime: number = this.timeService.currentTime;
        const time = +tvshow.start + ((tvshow.stop - tvshow.start) / 2);
        if (+tvshow.start <= currentTime && +tvshow.stop > currentTime) return this.getChannelFrame(tvshow.channel_id, currentTime);
        if (+tvshow.stop < currentTime) return this.getChannelFrame(tvshow.channel_id, time);
        return '';
    }
}