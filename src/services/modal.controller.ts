import { ModalSettings } from './../models/modal';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class ModalController {

    public modalState: EventEmitter<ModalSettings> = new EventEmitter<ModalSettings>();

    constructor() {
    }

    public create(settings: ModalSettings) {   
        this.modalState.next(settings);
    }

    public dismiss() {
        this.modalState.next(new ModalSettings(-1, 'channel'));
    }
}

