import { ContentInMemory } from './../models/memory';
import { Injectable } from '@angular/core';

@Injectable()

export class WatchMemoryService {

    constructor() {}

    setInMemory(item: ContentInMemory): void {
        const id: string = item.content_id.toString();
        if (this.isHaveInMemory) {
            localStorage.removeItem(id);
        }
        if (item.time == 0) {
          localStorage.removeItem(id);
        } else {
          localStorage.setItem(id, item.time.toString());
        }
    }

    getFromMemory(content_id: any): ContentInMemory {
        const id: string = content_id.toString();
        if (this.isHaveInMemory) {
            const value: string = localStorage.getItem(id);
            return new ContentInMemory(id, +value);
        } else {
            return new ContentInMemory(id, 0);
        }
    }

    private isHaveInMemory(content_id: any): boolean {
        const id: string = content_id.toString();
        const item = localStorage.getItem(id);
        if (item) return true;
        return false;
    }

}
