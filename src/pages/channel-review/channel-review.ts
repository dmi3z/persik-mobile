import { Subject } from 'rxjs/Subject';
import { VideoControlEventInfo, ControlEvents, PlayerEvents } from './../../models/player';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavParams, Slides, IonicPage, NavController } from 'ionic-angular';

import { ChannelsState } from '../../redux/channels/channels.state';
import { Channel } from './../../models/channel';
import { PlayerComponent } from './../../components/player/player';
import { Tvshow } from './../../models/tvshow';
import { DataService } from './../../services/data.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import * as moment from 'moment';
import { HeaderButton, HeaderButtonType } from '../../models/header';
import { ContentName } from '../../models/vod';
import { OrientationService } from '../../services/orientation.service';

@IonicPage()
@Component({
    selector: 'page-channel-review',
    templateUrl: './channel-review.html'
})

export class ChannelReviewPage {

    @ViewChild('date_filter_slider') dateFilterSlider: Slides;
    @ViewChild('player') player: PlayerComponent;
    @ViewChild('tvshows_area') tvshows_area: ElementRef;
    @ViewChild('switcher') switcher;
    @ViewChild('playerArea') playerArea;
    @ViewChild('dataFilter') dataFilter;

    private channel_id: number;
    private channel_index: number;
    private category_id: number;
    private isInited: boolean = false; // переключатель состояния страницы, становится true после того как страница попадает в массив роутов

    public currentChannel: Channel = new Channel();
    public tvshows: Tvshow[] = [];
    public dateScale: dateScaleItem[] = [];
    public today: number;
    public currentDay: dateScaleItem;
    public channels: Channel[] = [];
    public isShowControls: boolean = true;
    private controlsTimeout: any;
    public isPlaying: boolean = false;
    public isCanPlay: boolean = false;
    public isParentControlNeeded: boolean = false;

    private playerEventsSubscriber: Subscription;
    private checkPinEvent: Subject<boolean> = new Subject();

    public headerButtons: HeaderButtonType[] = [HeaderButton.BUTTON_BACK, HeaderButton.BUTTON_FAVORITE, HeaderButton.BUTTON_SEARCH];;

    readonly dateFilterSliderSettings = {
        slidesPerView: 3,
        spaceBetween: 15,
        centeredSlides: true,
        freeMode: false,
        initialSlide: 0
    }

    private channelStoreSubscriber: Subscription;
    private screenOrientationSubscriber: Subscription;
    private actualScreenOrientationSubscriber: Subscription;

    constructor(
        private navParams: NavParams,
        private channelsStore: Store<ChannelsState>,
        private dataService: DataService,
        private screenOrientation: ScreenOrientation,
        public navCtrl: NavController,
        private orientationService: OrientationService
    ) {
        this.today  = +moment().format('LL').split(' ')[0];
        this.initializeDateScale();
        this.channelStoreSubscriber = this.channelsStore.select('channelsData').subscribe(data => {
            this.channel_id = this.navParams.get('channel_id');
            this.category_id = this.navParams.get('category_id');
            if (this.category_id != 0 && this.category_id) {
                this.channels = data.channels.filter(channel => {
                    return channel.genres.find(genre => genre == this.category_id);
                });
            } else {
                this.channels = data.channels;
            }
            this.getCurrentChannel();
        });
    }

    ngOnInit() {
        this.showControls();
        this.initPlayerBehaviors();

        this.actualScreenOrientationSubscriber = this.orientationService.onChange.subscribe(res => {
            if (res == 'landscape') this.screenOrientation.unlock();
        });
    }

    ionViewDidLeave() {
        this.isInited = true;
        this.player.stop();
    }

    ionViewDidEnter() {
        if (this.isInited) {
            this.getCurrentChannel();
        }
    }

    ngAfterViewInit() {
        this.applySliderOptions();
    }

    public setFullscreen(): void {
        this.screenOrientation.lock('landscape');
    }

    public get isLandscape(): boolean {
        return this.screenOrientation.type.includes('landscape');
    }

    public loadNextChannel(): void {
        if (this.channels.length - 1 > this.channel_index) {
            this.channel_index++;
        } else {
            this.channel_index = 0;
        }
        this.onChannelChange();
    }

    public loadPreviousChannel(): void {
        if (this.channel_index >= 1) {
            this.channel_index--;
        } else {
            this.channel_index = this.channels.length - 1;
        }
        this.onChannelChange();
    }

    public setActiveDate(index: number) {
        this.dateFilterSlider.slideTo(index, 500);
        this.currentDay =  this.dateScale.find(item => item.id == this.dateFilterSlider.getActiveIndex());
        this.loadTvshows();
    }

    public dateSlideChange() {
        this.currentDay = this.dateScale.find(item => item.id == this.dateFilterSlider.getActiveIndex());
        this.loadTvshows();
    }

    public onChannelChange(channel?: Channel): void {
        if (channel) {
            this.currentChannel = channel;
        } else {
            this.currentChannel = this.channels[this.channel_index];
        }
        this.player.stop();

        this.isParentControlNeeded = this.isAdult;
        if (this.isParentControlNeeded) {
          this.checkPinEvent.subscribe(res => {
            this.playChannel();
          });
        } else {
          this.playChannel();
        }
        this.loadTvshows();
        this.showControls();
    }

    public onRedirect(data): void {
        this.navCtrl.push('VodInfoPage', data);
    }

    public showControls(): void {
        this.isShowControls = true;
        clearTimeout(this.controlsTimeout);
        this.controlsTimeout = setTimeout(() => {
            this.isShowControls = false;
        }, 4000);
    }

    public pause(event: Event): void {
        event.stopPropagation();
        this.player.pause();
        clearTimeout(this.controlsTimeout);
    }

    public resume(): void {
        this.player.resume();
        this.showControls();
    }

    public onControllerEvent(event: VideoControlEventInfo): void {
        switch (event.name) {
            case ControlEvents.CONTROL_PAUSE:
                this.player.pause();
                break;
            case ControlEvents.CONTROL_PLAY:
                this.player.resume();
                break;
            default:
                break;
        }
    }

    public onAdultCheckerComplete(result: boolean): void {
      if (result == true) {
        this.isParentControlNeeded = false;
        this.checkPinEvent.next(true);
      }
      if (!result) {
        this.isParentControlNeeded = false;
      }
    }

    private playChannel(): void {
      this.isParentControlNeeded = false;
      this.player.play(this.currentChannel.channel_id, ContentName.CHANNEL);
      this.isPlaying = true;
    }

    private get isAdult(): boolean {
      return +this.currentChannel.age_rating.substring(0, this.currentChannel.age_rating.length -1) == 18;
    }

    private initPlayerBehaviors() {                            // Подписка на события плеера
        this.playerEventsSubscriber = this.player.playerEvents.subscribe((event) => {
            switch (event) {
                case PlayerEvents.PLAYER_READY:
                    this.isPlaying = true;
                    this.isCanPlay = true;
                    break;
                case PlayerEvents.PLAYER_PAUSE:
                    this.isPlaying = false;
                    break;
                case PlayerEvents.PLAYER_PLAY:
                    this.isPlaying = true;
                    break;
                case PlayerEvents.PLAYER_ERROR_SUBSCRIPTION:
                    this.isCanPlay = false;
                    break;
                case PlayerEvents.PLAYER_ERROR_LOGIN:
                    this.isCanPlay = false;
                    break;
                default:
                    break;
            }
        });
    }

    private getCurrentChannel(): void {
        this.currentChannel = this.channels.find(data => data.channel_id == this.channel_id);
        this.channel_index = this.channels.indexOf(this.currentChannel);
        setTimeout(() => {
            this.player.stop();
            this.isParentControlNeeded = this.isAdult;
            if (this.isParentControlNeeded) {
              this.checkPinEvent.subscribe(res => {
                this.playChannel();
              });
            } else {
              this.playChannel();
            }
        }, 50);
        this.loadTvshows();
    }

    private initializeDateScale() {
        for (let i= -3; i < 4; i++) {
            const otherDay = moment().add(i, 'days');
            this.dateScale.push({
                id: i + 3,
                date: +otherDay.format('LL').split(' ')[0],
                day: otherDay.format('dd'),
                month: otherDay.format('LL').split(' ')[1],
                full_date: otherDay.format('YYYY-MM-DD')
            });
        }
        this.currentDay = this.dateScale.find(item => item.date == this.today);
    }

    private scrollToCurrentTvshow(): void {
        setTimeout(() => {
            const currentProgram: Element = document.getElementsByClassName('currentItem')[0];
            if (currentProgram) {
                currentProgram.scrollIntoView(true);
                this.tvshows_area.nativeElement.scrollBy(0, -100);
            }
        }, 100);
    }

    private loadTvshows() {
        this.dataService.getTvshows(this.currentChannel.channel_id, this.currentDay.full_date).then(data => {
            this.tvshows = data;
            if (data.length > 0) {
                this.scrollToCurrentTvshow();
            }
        });
    }

    private applySliderOptions() {
        if (this.dateFilterSlider) {
            this.dateFilterSliderSettings.initialSlide = this.dateScale.find(item => item.date == this.today).id; // Инициализация сегодняшнего дня в ленте
            Object.assign(this.dateFilterSlider, this.dateFilterSliderSettings);                                  // -1 поправка на центрирование слайдов
        }
    }

    ngOnDestroy() {
        this.screenOrientation.unlock();
        if (this.screenOrientationSubscriber) this.channelStoreSubscriber.unsubscribe();
        if (this.channelStoreSubscriber) this.channelStoreSubscriber.unsubscribe();
        if (this.playerEventsSubscriber) this.playerEventsSubscriber.unsubscribe();
        if (this.actualScreenOrientationSubscriber) this.actualScreenOrientationSubscriber.unsubscribe();
    }

}

interface dateScaleItem {
    id: number;
    date: number;
    month: string;
    day: string;
    full_date: string;
}
