import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChannelReviewPage } from './channel-review';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ChannelReviewPage
  ],
  imports: [
    IonicPageModule.forChild(ChannelReviewPage),
    ComponentsModule
  ],
  exports: [
    ChannelReviewPage
  ]
})

export class ChannelReviewPageModule {}
