import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from '../../components/components.module';
import { PladformPlayerPage } from './pladform-player';

@NgModule({
  declarations: [
    PladformPlayerPage
  ],
  imports: [
    IonicPageModule.forChild(PladformPlayerPage),
    ComponentsModule
  ],
  exports: [
    PladformPlayerPage
  ]
})

export class PladformPlayerPageModule {}
