import { HeaderButtonType } from './../../models/header';
import { NavParams, IonicPage } from 'ionic-angular';
import { Component, OnInit, OnDestroy } from "@angular/core";
import { HeaderButton } from '../../models/header';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

@IonicPage()
@Component({
  selector: 'page-pladform-player',
  templateUrl: './pladform-player.html'
})

export class PladformPlayerPage implements OnInit, OnDestroy {

  public headerButtons: HeaderButtonType[] = [HeaderButton.BUTTON_BACK, HeaderButton.BUTTON_FAVORITE, HeaderButton.BUTTON_SEARCH];

  constructor(private navParams: NavParams, private screenOrientation: ScreenOrientation) {}

  ngOnInit() {
    this.screenOrientation.unlock();
    const id: number = +this.navParams.get('id');
    this.createIFrame(id);
  }

  public get isLandscape(): boolean {
    return this.screenOrientation.type.includes('landscape');
  }

  private createIFrame(id: number): void {
    const frame = document.createElement('iframe');
    const pladform = document.getElementById('pladform');
    frame.setAttribute('src', `//out.pladform.ru/player?pl=45367&videoid=${id}`);
    frame.setAttribute('webkitAllowFullScreen', '');
    frame.setAttribute('mozallowfullscreen', '');
    frame.setAttribute('allowfullscreen', '');
    frame.setAttribute('frameborder', '0');
    frame.className = '_3WIHbcqSpZ0sOHly _1uSNPo8nDaWhWQnE';
    frame.style.width = '100%';
    frame.style.height = '100%';
    pladform.appendChild(frame);
  }

  ngOnDestroy() {
    this.screenOrientation.unlock();
  }
}
