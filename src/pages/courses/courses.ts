import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { Genre } from './../../models/category';
import { DataService } from '../../services/data.service';
import { VodState } from './../../redux/vod/vod.state';
import { HeaderButton, HeaderButtonType } from '../../models/header';

@IonicPage()
@Component({
    selector: 'courses',
    templateUrl: './courses.html'
})

export class CoursesPage {

    readonly loadPart: number = 15; // Количество разово загружаемых видео на отображение в ленте

    public contentFeed = [];
    public genres: Genre[] = [];

    public headerButtons: HeaderButtonType[] = [HeaderButton.BUTTON_MENU, HeaderButton.BUTTON_FAVORITE, HeaderButton.BUTTON_SEARCH];

    private category_id: number;
    private selectedGenreId: number;
    private totalCount: number;
    private skip: number = 0;

    private vodStoreSubscriber: Subscription;

    constructor(private vodStore: Store<VodState>, private dataService: DataService) {}

    ngOnInit() {
        this.vodStoreSubscriber = this.vodStore.select('vodCategories').subscribe(data => {
            this.category_id = data.categories[4].id;
            this.genres = data.categories[0].genres.filter(genre => genre.is_main);
            this.genres.unshift({
                id: 0,
                is_main: true,
                name: 'Все жанры',
                name_en: 'All genres'
            });
            this.selectedGenreId = this.genres[0].id;
            this.loadContent();
        });
    }

    private loadContent(): void {
        this.dataService.loadVideoContent(this.category_id, this.selectedGenreId, this.loadPart, this.skip).then(result => {
            this.totalCount = result.total;
            this.contentFeed.push(...result.videos);
        });
    }

    public changeFilter(genre_id: number): void {
        this.selectedGenreId = genre_id;
        this.contentFeed = [];
        this.skip = 0;
        this.loadContent();
    }

    public doInfinite(e): void { // Метод бесконечного скролла
        this.skip += this.loadPart;
        if ((this.totalCount - this.skip) > 0) {
            this.loadContent();
        }
        e.complete();
    }

    ngOnDestroy() {
        if (this.vodStoreSubscriber) this.vodStoreSubscriber.unsubscribe();
    }

}
