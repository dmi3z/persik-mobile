import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { IonicPage, NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { ModalController } from './../../services/modal.controller';
import { ModalSettings } from './../../models/modal';
import { AuthService } from './../../services/auth.service';
import { FavoriteService } from './../../services/favorite.service';
import { Genre } from './../../models/category';
import { Channel } from './../../models/channel';
import { ChannelsState } from '../../redux/channels/channels.state';
import { HeaderButton, HeaderButtonType } from '../../models/header';
import { ContentName } from '../../models/vod';

@IonicPage()
@Component({
    selector: 'page-tv',
    templateUrl: './tv.html'
})

export class TvPage {

    public channelsFeed: Channel[] = [];
    public headerButtons: HeaderButtonType[] = [HeaderButton.BUTTON_MENU, HeaderButton.BUTTON_FILTER, HeaderButton.BUTTON_FAVORITE, HeaderButton.BUTTON_SEARCH];;
    public categories: Genre[] = [];

    private channels: Channel[] = [];
    private selectedCategoryId: number = 0;

    private channelsStoreSubscriber: Subscription;

    constructor(
        private channnelsStore: Store<ChannelsState>,
        public navCtrl: NavController,
        private favoriteService: FavoriteService,
        private authService: AuthService,
        private modalCtrl: ModalController
    ) {}

    ngOnInit() {
        this.channelsStoreSubscriber = this.channnelsStore.select('channelsData').subscribe(data => {
            this.channels = data.channels;
            this.categories = data.categories;
            this.showFilteredChannels(0);
        });
    }

    public showChannelReview(channel_id: number) {
        this.navCtrl.push('ChannelReviewPage', { channel_id: channel_id,  category_id: this.selectedCategoryId });
    }

    public showFilteredChannels(categoryId: number) {
        if (categoryId == 0) {
            this.channelsFeed = this.channels;
        } else {
            this.channelsFeed = this.channels.filter(channel => {
                return channel.genres.find(genre => genre == categoryId); 
             });
        }
        this.selectedCategoryId = categoryId;
    }

    public isFavorite(channel_id: number): Observable<boolean> {
        return this.favoriteService.isChannelFavorite(channel_id);
    }

    public get isLogin(): boolean {
        return this.authService.isLogin;
    }

    public removeFavorite(event: Event, channel_id: number): void {
        event.stopPropagation();
        this.favoriteService.deleteFromFavorite(new ModalSettings(channel_id, ContentName.CHANNEL));
    }

    public showModal(channel_id: number) {
        this.modalCtrl.create(new ModalSettings(channel_id, ContentName.CHANNEL));
    }

    ngOnDestroy() {
        if (this.channelsStoreSubscriber) this.channelsStoreSubscriber.unsubscribe();
    }

}
