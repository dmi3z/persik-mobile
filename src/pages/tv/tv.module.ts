import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TvPage } from './tv';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    TvPage
  ],
  imports: [
    IonicPageModule.forChild(TvPage),
    ComponentsModule
  ],
  exports: [
    TvPage
  ]
})

export class TvPageModule {}
