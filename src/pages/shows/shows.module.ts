import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowsPage } from './shows';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ShowsPage
  ],
  imports: [
    IonicPageModule.forChild(ShowsPage),
    ComponentsModule
  ],
  exports: [
    ShowsPage
  ]
})

export class ShowsPageModule {}
