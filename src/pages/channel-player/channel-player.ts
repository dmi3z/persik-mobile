import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from './../../services/auth.service';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { PlayerComponent } from './../../components/player/player';
import { ContentName } from '../../models/vod';
import { Channel } from '../../models';
import { Observable } from 'rxjs/Observable';
import { HeaderButtonType, HeaderButton } from './../../models/header';
import { Store } from '@ngrx/store';
import { ChannelsState } from './../../redux/channels/channels.state';
import { Tvshow } from './../../models/tvshow';
import { TimeService } from './../../services/time.service';
import { DataService } from './../../services/data.service';
import { PlayerEvents, VideoControlEventInfo, ControlEvents } from '../../models/player';

@IonicPage()
@Component({
    selector: 'page-channel-player',
    templateUrl: './channel-player.html'
})

export class ChannelPlayerPage {

    public headerButtons: HeaderButtonType[] = [HeaderButton.BUTTON_BACK, HeaderButton.BUTTON_FAVORITE, HeaderButton.BUTTON_SEARCH];
    public currentChannel: Channel;
    public tvshows: Tvshow[] = [];
    public isPlaying: boolean = false;
    public isCanPlay: boolean = false;
    public isParentControlNeeded: boolean = false;

    @ViewChild('player') player: PlayerComponent;
    private channel_id: number;
    private isPageInited: boolean = false;
    private channelStoreSubscriber: Subscription;
    private playerEventsSubscriber: Subscription;
    private checkPinEvent: Subject<boolean> = new Subject();
    private checkPinSubscriber: Subscription;

    constructor(
        private navParams: NavParams,
        private channelStore: Store<ChannelsState>,
        private dataService: DataService,
        private timeService: TimeService,
        private screenOrientation: ScreenOrientation,
        private authService: AuthService
    ) {}

    ngOnInit() {
        this.channel_id = +this.navParams.get('channel_id');
        this.initializeChannel();

        this.initPlayerBehaviors();
    }

    private initPlayerBehaviors() {                            // Подписка на события плеера
        this.playerEventsSubscriber = this.player.playerEvents.subscribe((event) => {
            switch (event) {
                case PlayerEvents.PLAYER_READY:
                    this.isPlaying = true;
                    this.isCanPlay = true;
                    break;
                case PlayerEvents.PLAYER_PAUSE:
                    this.isPlaying = false;
                    break;
                case PlayerEvents.PLAYER_PLAY:
                    this.isPlaying = true;
                    break;
                case PlayerEvents.PLAYER_ERROR_SUBSCRIPTION:
                  this.isCanPlay = false;
                  break;
                case PlayerEvents.PLAYER_ERROR_LOGIN:
                  this.isCanPlay = false;
                  break;
                default:
                    break;
            }
        });
    }

    public onControlEvent(event: VideoControlEventInfo): void {                     // Подписка на события контрола
        switch (event.name) {
            case ControlEvents.CONTROL_PAUSE:
                this.player.pause();
                break;
            case ControlEvents.CONTROL_PLAY:
                this.player.resume();
                break;
            default:
                break;
        }
    }

    public onAdultCheckerComplete(result: boolean): void {
      if (result == true) {
        this.isParentControlNeeded = false;
        this.checkPinEvent.next(true);
      }
      if (!result) {
        this.isParentControlNeeded = false;
      }
    }

    ionViewDidLeave(){
        this.player.pause();
    }

    ionViewWillEnter() {
        if (this.isPageInited) {
            this.player.resume();
        } else {
            this.initializeChannel();
        }
    }

    public get channels(): Observable<Channel[]> {
        return this.channelStore.select('channelsData').map(res => res.channels);
    }

    public changeActiveChannel(channel: Channel): void {
        this.channel_id = channel.channel_id;
        this.loadChannelById(channel.channel_id);
        this.loadChannelTvshows(channel.channel_id);
        this.player.stop();
        this.playChannel();
    }

    public get isLandscape(): boolean {
        return this.screenOrientation.type.includes('landscape');
    }

    private get isAdult(): boolean {
      return +this.currentChannel.age_rating.substring(0, this.currentChannel.age_rating.length -1) == 18;
    }

    private initializeChannel(): void {
        if (this.channel_id) {
            this.loadChannelById(this.channel_id);
            this.loadChannelTvshows(this.channel_id);
            this.playChannel();
            if (this.authService.isLogin) this.isPageInited = true;
        }
    }

    private playChannel(): void {
      this.isParentControlNeeded = this.isAdult;
      if (!this.isParentControlNeeded)  {
        if (this.checkPinSubscriber) this.checkPinSubscriber.unsubscribe();
        this.player.play(this.channel_id, ContentName.CHANNEL);
      } else {
        this.checkPinSubscriber = this.checkPinEvent.subscribe(res => {
          this.player.play(this.channel_id, ContentName.CHANNEL);
        });
      }
    }

    private loadChannelById(id: number): void {
        this.channelStoreSubscriber = this.channelStore.select('channelsData').subscribe(res => {
            this.currentChannel = res.channels.find(ch => ch.channel_id == id);
        });
    }

    private loadChannelTvshows(channel_id: number): void {
        this.dataService.getTvshows(channel_id, this.timeService.getDate()).then(res => {
            this.tvshows = res;
        });
    }

    ngOnDestroy() {
        this.player.stop();
        if (this.channelStoreSubscriber) this.channelStoreSubscriber.unsubscribe();
        if (this.playerEventsSubscriber) this.playerEventsSubscriber.unsubscribe();
        if (this.checkPinSubscriber) this.checkPinSubscriber.unsubscribe();
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
}
