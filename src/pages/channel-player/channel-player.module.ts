import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChannelPlayerPage } from './channel-player';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ChannelPlayerPage
  ],
  imports: [
    IonicPageModule.forChild(ChannelPlayerPage),
    ComponentsModule
  ],
  exports: [
    ChannelPlayerPage
  ]
})

export class ChannelPlayerPageModule {}
