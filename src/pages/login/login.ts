import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { AuthService } from './../../services/auth.service';
import { authUser } from '../../models/user';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: './login.html'
})

export class LoginPage {

    public email: string = '';
    public password: string = '';
    public confirm_password: string = '';
    public isLoginState: boolean = true;

    public isEmailValid: boolean = true;
    public isEmailExist: boolean = true;
    public isPassValid: boolean = true;
    public isPassLengthValid: boolean = true;
    public isConfPassValid: boolean = true;
    public isEmailFieldEmpty: boolean = false;
    public isPassFieldEmpty: boolean = false;
    public isConfPassFieldEmpty: boolean = false;
    public isNewEmailExist: boolean = false;

    constructor(public navCtrl: NavController, private authService: AuthService, private ga: GoogleAnalytics) {}

    ionViewCanEnter() {
        return !this.authService.isLogin;
    }

    public goBack(): void {
        if (this.navCtrl.canGoBack()) {
            this.navCtrl.pop();
        } else {
            this.navCtrl.setRoot('HomePage');
        }
    }

    public checkEmail(): void {
        this.authService.checkEmail(this.email).then(res => {
            this.isEmailValid = true;
            this.isEmailExist = res.exists;
        },
        () => {
            this.isEmailValid = false;
        });
    }

    public checkExistEmail() {
        this.authService.checkEmail(this.email).then(res => {
            this.isEmailValid = true;
            this.isNewEmailExist = res.exists;
        },
        () => {
            this.isEmailValid = false;
        });
    }

    public checkPassword() {
        if (!this.password || this.password.length == 0) {
            this.isPassFieldEmpty = true;
        }
    }

    public clearAll() {
        this.isEmailValid = true;
        this.isEmailExist = true;
        this.isPassValid = true;
        this.isPassLengthValid = true;
        this.isConfPassValid = true;
        this.isEmailFieldEmpty = false;
        this.isPassFieldEmpty = false;
        this.isConfPassFieldEmpty = false;
        this.isNewEmailExist = false;
    }

    public checkConfirm() {
        if (this.password == this.confirm_password) {
            this.isConfPassValid = true;
        } else {
            this.isConfPassValid = false;
        }
    }

    public login() {
        if (this.email.length >= 5 && this.password.length != 0) {
            this.authService.login(this.email, this.password).then(res => {
                this.authSuccess(res);
            }, () => {
                this.checkEmail();
                this.isPassValid = false;
            });
        } else {
            if (this.email.length < 5) this.isEmailFieldEmpty = true;
            if (this.password.length == 0) this.isPassFieldEmpty = true;
        }
    }

    public register() {
        if (this.email.length >= 5 && this.password.length >= 6 && this.confirm_password.length > 0 && this.isConfPassValid) {
            this.authService.register(this.email, this.password).then(() => {
                this.login();
                this.ga.trackView('register');
            }, () => {
                this.checkExistEmail();
                this.isPassValid = false;
            });
        } else {
            if (this.email.length < 5) this.isEmailFieldEmpty = true;
            if (this.password.length == 0) this.isPassFieldEmpty = true;
            if (this.password.length < 6 && this.password.length > 0) this.isPassLengthValid = false;
            if (this.password != this.confirm_password) this.isConfPassValid = false;
        }
    }

    private authSuccess(result: authUser) {
        this.authService.token = result.auth_token;
        this.authService.user_id = result.user_id;
        this.authService.changeLoginState();
        this.navCtrl.pop();
    }

}
