import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { HeaderButton, HeaderButtonType } from '../../models/header';
import { Tariff, UserSubscription, Payment, PaymentInfo } from './../../models/user';
import { AuthService } from './../../services/auth.service';

@IonicPage()
@Component({
    selector: 'page-billing-confirm',
    templateUrl: './billing-confirm.html'
})

export class BillingConfirmPage {

    public selectedTariffs: Tariff[] = [];
    public payWays: string[] = [];
    public headerButtons: HeaderButtonType[] = [HeaderButton.BUTTON_BACK, HeaderButton.BUTTON_SEARCH, HeaderButton.BUTTON_FAVORITE];
    public isShowPayModal: boolean = false;
    public payModalInfo: PaymentInfo;

    private user_subscriptions: UserSubscription;
    private tariff: Tariff;
    private payIcons = {
        assist: './assets/pay-icons/assist.png',
        mts: './assets/pay-icons/mts.png',
        life: './assets/pay-icons/life.png',
        erip: './assets/pay-icons/erip.png',
        yandex: './assets/pay-icons/yandex.png',
        ipay: './assets/pay-icons/ipay.png',
    };

    constructor(private navParams: NavParams, private authService: AuthService, private ga: GoogleAnalytics) {}

    ngOnInit() {
        this.tariff = this.navParams.get('tariff');
        this.user_subscriptions = this.navParams.get('user_subscriptions');
        this.loadPayWays();
        if (this.isHaveExtendSubscriptions) {
            this.convertSubscriptionsToTariffs().then(res => this.selectedTariffs = res);
        } else if (this.tariff) {
            this.selectedTariffs.push(this.tariff);
        }
        this.ga.trackView('billing');
    }

    public get billingSumm(): number {
        let summ = 0;
        this.selectedTariffs.forEach(t => {
            summ += t.options[0].price;
        });
        return Math.round(summ * 100) / 100;
    }

    public getPayName(payCode: string): any[] {
        switch (payCode) {
            case 'assist': return ['Оплатить с помощью Ассист', this.payIcons.assist];
            case 'ipay_mts': return ['Оплатить через IPay МТС', this.payIcons.mts, this.payIcons.ipay];
            case 'ipay_life': return ['Оплатить через IPay Life:)', this.payIcons.life, this.payIcons.ipay];
            case 'ipay_erip': return ['Оплатить через IPay ЕРИП', this.payIcons.erip, this.payIcons.ipay];
            case 'yandex': return ['Оплатить с помощью системы Яндекс.Касса', this.payIcons.yandex];
            default: break;
        }
    }

    public createPayment(pay_sys: string): void {
        const ids: number[] = this.selectedTariffs.map(t => t.options[0].product_option_id);
        this.authService.createPayment(new Payment(pay_sys, ids)).then(data => {
            this.payModalInfo = data;
            this.isShowPayModal = true;
        });
    }

    public get isHaveDescription(): boolean {
      return this.payModalInfo && this.payModalInfo.description && this.payModalInfo.description.length > 0;
    }

    private loadPayWays(): void {
      this.authService.getTariffs().then(t => this.payWays = t.pay_sys);
    }

    private convertSubscriptionsToTariffs(): Promise<Tariff[]> {
        return this.authService.getTariffs().then(tariff => {
            /* return tariff.products.filter(p => {
                return this.user_subscriptions.find(us => us.product_id == p.product_id);
            }); */
            return tariff.products.filter(p => p.product_id == this.user_subscriptions.product_id);
        });
    }


    private get isHaveExtendSubscriptions(): boolean {
        // return this.user_subscriptions && this.user_subscriptions.length > 0;
        if (this.user_subscriptions) return true;
        return false;
    }

    public closePayModal(): void {
        this.isShowPayModal = false;
    }

    public stopProp(event): void {
        event.stopPropagation();
    }

}
