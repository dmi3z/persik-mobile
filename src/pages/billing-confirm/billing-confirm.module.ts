import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BillingConfirmPage } from './billing-confirm';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    BillingConfirmPage
  ],
  imports: [
    IonicPageModule.forChild(BillingConfirmPage),
    ComponentsModule
  ],
  exports: [
    BillingConfirmPage
  ]
})

export class BillingConfirmPageModule {}
