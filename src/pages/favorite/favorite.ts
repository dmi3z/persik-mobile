import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { FavoritesState } from '../../redux/favorite/favorite.state';
import { Subscription } from 'rxjs/Subscription';
import { FavoriteChannel } from '../../models/favorite';
import { HeaderButton, HeaderButtonType } from '../../models/header';
import { VodSection, ContentName } from './../../models/vod';
import { FavoriteVideo, FavoriteTvshow } from './../../models/favorite';
import { ChannelsState } from './../../redux/channels/channels.state';
import { Channel } from './../../models/channel';

@IonicPage()
@Component({
    selector: 'page-favorite',
    templateUrl: './favorite.html'
})

export class FavoritePage {

    public headerButtons: HeaderButtonType[] = [HeaderButton.BUTTON_MENU, HeaderButton.BUTTON_FAVORITE, HeaderButton.BUTTON_SEARCH];;

    public favoriteChannels: Channel[] = [];
    public favoriteVideos: VodSection;
    public favoriteTvshows: VodSection;

    public featuredChannelsTitle: string = 'Избранные каналы';
    public featuredVideosTitle: string = 'Избранные видео';
    public featuredTvshowsTitle: string = 'Избранные передачи';

    private favoriteStoreSubscriber: Subscription;
    private channelStoreSubscriber: Subscription;

    constructor(
        public navCtrl: NavController,
        private favoriteStore: Store<FavoritesState>,
        private channelStore: Store<ChannelsState>
    ) {}

    ngOnInit() {
        this.favoriteStoreSubscriber = this.favoriteStore.select('favoritesData').subscribe(f_content => {
            this.loadFavoriteChannels(f_content.channels);
            this.loadFavoriteVideos(f_content.videos);
            this.loadFavoriteTvshows(f_content.tvshows);
        });
    }

    public get isHaveFavoriteChannels(): boolean {
        return this.favoriteChannels.length > 0;
    }

    public get isHaveFavoriteVideos(): boolean {
        return this.favoriteVideos && this.favoriteVideos.videos.length > 0;
    }
    public get isHaveFavoriteTvshows(): boolean {
        return this.favoriteVideos && this.favoriteTvshows.videos.length > 0;
    }

    private loadFavoriteChannels(f_channels: FavoriteChannel[]): void {
        if (this.channelStoreSubscriber) this.channelStoreSubscriber.unsubscribe();
        this.channelStoreSubscriber = this.channelStore.select('channelsData').subscribe((ch) => {
            this.favoriteChannels = ch.channels.filter(channel => {
                return f_channels.find(f_channel => f_channel.channel_id == channel.channel_id);
            });
        });
    }

    private loadFavoriteVideos(f_videos: FavoriteVideo[]): void {
        if (f_videos.length > 0) {
                this.favoriteVideos = {
                type: ContentName.VIDEO,
                videos: f_videos.map(video => {
                    return { video_id: video.video_id }; 
                })
            };
        } else {
            this.favoriteVideos = {
                type: ContentName.VIDEO,
                videos: []
            }
        }
    }

    private loadFavoriteTvshows(f_tvshows: FavoriteTvshow[]): void {
        if (f_tvshows.length > 0) {
            this.favoriteTvshows = {
                type: ContentName.TV,
                videos: f_tvshows.map(tv => {
                    return { tvshow_id: tv.tvshow_id };
                })
            };
        } else {
            this.favoriteTvshows = {
                type: ContentName.TV,
                videos: []
            };
        }
    }
    
    ngOnDestroy() {
        if (this.channelStoreSubscriber) this.channelStoreSubscriber.unsubscribe();
        if (this.favoriteStoreSubscriber) this.favoriteStoreSubscriber.unsubscribe();
    }

}