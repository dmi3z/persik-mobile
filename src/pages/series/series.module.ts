import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SeriesPage } from './series';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
      SeriesPage
  ],
  imports: [
    IonicPageModule.forChild(SeriesPage),
    ComponentsModule
  ],
  exports: [
    SeriesPage
  ]
})

export class SeriesPageModule {}
