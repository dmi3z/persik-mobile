export { SearchPage } from './search/search';
export { FavoritePage } from './favorite/favorite';
export { HomePage } from './home/home';
export { FilmsPage } from './films/films';
export { CartoonsPage } from './cartoons/cartoons';
export { VodInfoPage } from './vod-info/vod-info';
