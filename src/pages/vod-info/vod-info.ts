import { Subscription } from 'rxjs/Subscription';
import { TimeService } from './../../services/time.service';
import { Component } from '@angular/core';
import { IonicPage, NavParams, NavController } from 'ionic-angular';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Video, ContentName } from '../../models/vod';
import { ModalSettings } from '../../models/modal';
import { ChannelsState } from '../../redux/channels/channels.state';
import { HeaderButtonType, HeaderButton } from '../../models/header';
import { Episode, ContentType } from './../../models/vod';
import { DataService } from './../../services/data.service';
import { FavoriteService } from './../../services/favorite.service';
import { AuthService } from './../../services/auth.service';
import { Platform } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-vod-info',
    templateUrl: './vod-info.html'
})

export class VodInfoPage {

    public vodInfo: Video;
    public genres: string;
    public type: ContentType;
    public tvshow_id: string;
    public video_id: number;
    public headerButtons : HeaderButtonType[] = [HeaderButton.BUTTON_BACK, HeaderButton.BUTTON_FAVORITE, HeaderButton.BUTTON_SEARCH];
    public isAvailable: boolean = true;
    public isCanPlay: boolean = true;
    public descrText: any;

    private activeSeason: string;
    private channelsStoreSubscriber: Subscription;
    private isPlayableSubscriber: Subscription;
    private showFullDescrBtn: Element;

    constructor(
        private navParams: NavParams,
        private authService: AuthService,
        private favoriteService: FavoriteService,
        public navCtrl: NavController,
        private dataService: DataService,
        private channelsStore: Store<ChannelsState>,
        private timeService: TimeService,
        public platform: Platform
    ) {}

    ngOnInit() {
        this.vodInfo = this.navParams.get('info');
        const genresArray: string[] = this.navParams.get('genres');
        this.type = this.navParams.get('type');
        this.tvshow_id = this.navParams.get('tvshow_id');
        this.video_id = +this.navParams.get('video_id');
        this.isPlayable.then(res => {
            this.isCanPlay = res;
            if (this.isPlayableSubscriber) this.isPlayableSubscriber.unsubscribe();
        });
        this.genres = this.getGenresString(genresArray);
        this.checkAvailable();
        this.makeDescrText(this.vodInfo.description);
    }

    ngAfterViewInit() {
        this.showFullDescrBtn = document.getElementsByClassName('showFullDescr')[0];
        if (this.showFullDescrBtn) {
            this.showFullDescrBtn.addEventListener('click', this.showAllTextHandler.bind(this));
        }
    }

    public get uniqueSeasons(): string[] { // Получение массива уникальных названий сезонов
        return this.vodInfo.episodes.map(item => item.season).filter((item, index, self) => self.indexOf(item) == index).sort();
    }

    public getSeriesBySeason(seasonName: string): Episode[] { // Получение серий по номеру сезона
        return this.vodInfo.episodes.filter(item => item.season == seasonName).sort((a, b) => {
            return +a.episode.split(' ')[1] - +b.episode.split(' ')[1];
        });
    }

    public getSeriesCount(seasonName: string): string {
        const count = this.vodInfo.episodes.filter(item => item.season == seasonName).length;
        return `${count} сери${this.getSeriesCase(count)}`;
    }

    public setActiveSeason(season): void {
        this.activeSeason = season == this.activeSeason ? null : season;
    }

    public playSeries(series: Episode) {
      if (series.video_id) {
        this.navCtrl.push('VideoPlayerPage', { id: series.video_id, type: series.type });
      } else {
        this.navCtrl.push('VideoPlayerPage', { id: series.tvshow_id, type: ContentName.TV });
      }
    }

    public isActiveSeason(season): boolean {
        return this.activeSeason == season;
    }

    public isSeriesFavorite(series: Episode): Observable<boolean> {
        return this.favoriteService.isVideoFavorite(series.video_id);
    }

    public get isSeries(): boolean {
        return this.vodInfo.is_series;
    }

    public get isLogin(): boolean {
        return this.authService.isLogin;
    }

    public get isHaveDuration(): boolean {
        return this.vodInfo.duration && this.vodInfo.duration != 0;
    }

    public get isFavorite(): Observable<boolean> {
        if (this.type == ContentName.TV) return this.favoriteService.isTvshowFavorite(this.tvshow_id);
        if (this.type == ContentName.VIDEO) return this.favoriteService.isVideoFavorite(this.video_id);
    }

    public get isTablet(): boolean {
        return this.platform.is("tablet");
    }

    private get isPlayable(): Promise<boolean> { // Метод только для проверки возможности воспроизведения передачи в архиве
        return new Promise(observer => {
            if (this.type == ContentName.TV) {                                                                    // Если проверяемым контентом является передача
                this.dataService.getTvshowInfo(this.tvshow_id).then(t => {                                        // Находим передачу по ее id
                    this.isPlayableSubscriber = this.channelsStore.select('channelsData').subscribe(data => {     // Находим канал на котором воспроизводилась данная передача
                        const dvr: number = data.channels.find(ch => ch.channel_id == t.channel_id).dvr_sec;      // Определяем глубину записи канала
                        const currentTime: number = this.timeService.currentTime;
                        if (t.start >= (currentTime - dvr) && t.stop < currentTime) {                             // Если начало передачи вписывается в рамки глубины записи то разрешаем воспроизведение
                            observer(true);
                        } else {
                            observer(false);
                        }
                    });
                });
            } else {
                observer(true);
            }
        });
    }

    public addToFavorite(): void {
        if (this.type == ContentName.TV) this.favoriteService.addToFavorite(new ModalSettings(this.tvshow_id, ContentName.TV));
        if (this.type == ContentName.VIDEO) this.favoriteService.addToFavorite(new ModalSettings(this.video_id, ContentName.VIDEO));
    }

    public removeFromFavorite(): void {
        if (this.type == ContentName.TV) this.favoriteService.deleteFromFavorite(new ModalSettings(this.tvshow_id, ContentName.TV));
        if (this.type == ContentName.VIDEO) this.favoriteService.deleteFromFavorite(new ModalSettings(this.video_id, ContentName.VIDEO));
    }

    public showVideo(): void {
      if (this.vodInfo.is_pladform) {
        this.navCtrl.push('PladformPlayerPage', { id: this.vodInfo.pladform_id });
      } else {
        if (!this.isSeries) {
          const id = this.tvshow_id ? this.tvshow_id : this.video_id;
          const type = this.tvshow_id ? ContentName.TV : ContentName.VIDEO;
          this.navCtrl.push('VideoPlayerPage', { id, type });
        } else {                                                                 // Если сериал то находим первую серию первого сезона и запускаем
          const series: Episode = this.getSeriesBySeason(this.uniqueSeasons[0])[0];
          if (series.video_id) {
            this.navCtrl.push('VideoPlayerPage', { id: series.video_id, type: series.type });
          } else {
            this.navCtrl.push('VideoPlayerPage', { id: series.tvshow_id, type: series.type });
          }
        }
      }
    }

    public redirectToLoginPage(): void {
        this.navCtrl.push('LoginPage');
    }

    public redirectToSubscribePage(): void {
        this.navCtrl.push('AccountPage');
    }

    private getSeriesCase(count: number): string {
        const countString = count.toString();
        if (+countString.slice(-1) == 1) return 'я';
        if (+countString.slice(-1) >= 2 && +countString.slice(-1) <= 4) return 'и';
        if (+countString.slice(-1) > 4 || +countString.slice(-1) == 0) return 'й';
    }

    private getGenresString(genres: string[]): string {
        if (genres) {
            let genresString = '';
            genres.forEach(genre => {
                const genreName: string = genre[0].toUpperCase() + genre.slice(1);
                if (genresString.length == 0) {
                    genresString = genreName;
                } else {
                    genresString += `, ${genreName}`;
                }
            });
            return genresString;
        }
        return null;
    }

    private checkAvailable(): void {
        if (this.type == 'tv') {
            this.dataService.getTvshowInfo(this.tvshow_id).then(res => {
                this.channelsStoreSubscriber = this.channelsStore.select('channelsData').subscribe(data => {
                    const channel = data.channels.find(ch => ch.channel_id == res.channel_id);
                    this.isAvailable = channel.available;
                });
            });
        } else {
            this.isAvailable = true;
        }
    }

    private makeDescrText(text: string): void {
        if (text.length > 250 && !this.isTablet) {
            let partOfText = text.substr(0, 250);
            this.descrText = `${partOfText}...<span class="showFullDescr">Читать все</span>`;
        } else {
            this.descrText = text;
        }
    }

    private showAllTextHandler(): void {
      this.descrText = this.vodInfo.description;
    }

    ngOnDestroy() {
        if (this.isPlayableSubscriber) this.isPlayableSubscriber.unsubscribe();
        if (this.channelsStoreSubscriber) this.channelsStoreSubscriber.unsubscribe();
        if (this.showFullDescrBtn) this.showFullDescrBtn.removeEventListener('click', this.showAllTextHandler.bind(this));
    }
}
