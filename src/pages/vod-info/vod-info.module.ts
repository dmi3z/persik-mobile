import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VodInfoPage } from './vod-info';

import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    VodInfoPage
  ],
  imports: [
    IonicPageModule.forChild(VodInfoPage),
    ComponentsModule
  ],
  exports: [
    VodInfoPage
  ]
})

export class VodInfoPageModule {}
