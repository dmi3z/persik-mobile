import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CoursePaymentPage } from './course-payment';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CoursePaymentPage
  ],
  imports: [
    IonicPageModule.forChild(CoursePaymentPage),
    ComponentsModule
  ],
  exports: [
    CoursePaymentPage
  ]
})

export class CoursePaymentPageModule {}
