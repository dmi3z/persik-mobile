import { PaymentInfo } from './../../models/user';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { AuthService } from '../../services/auth.service';
import { Tariff, Payment } from '../../models/user';

import { ScreenOrientation } from '@ionic-native/screen-orientation';

@IonicPage()
@Component({
  selector: 'course-payment',
  templateUrl: './course-payment.html'
})

export class CoursePaymentPage implements OnInit, OnDestroy {

  public payment: Tariff;
  public payWays: string[] = [];
  public isShowPayModal: boolean = false;
  public payModalInfo: PaymentInfo;

  private course_id: number;
  private payIcons = {
    assist: './assets/pay-icons/assist.png',
    mts: './assets/pay-icons/mts.png',
    life: './assets/pay-icons/life.png',
    erip: './assets/pay-icons/erip.png',
    yandex: './assets/pay-icons/yandex.png',
    ipay: './assets/pay-icons/ipay.png',
  };

  constructor(private navParams: NavParams, private authService: AuthService, private screenOrientation: ScreenOrientation) { }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.course_id = +this.navParams.get('id');
    this.authService.getAllTariffs().then(res => {
      const option = res.products.find(p => p.options.some(o => +o.product_option_id === this.course_id));
      this.payment = option;
      this.loadPayWays();
    });
  }

  public createPayment(pay_sys: string): void {
    this.authService.createPayment(new Payment(pay_sys, [this.course_id])).then(data => {
      this.payModalInfo = data;
      this.isShowPayModal = true;
    });
  }

  public get isHaveDescription(): boolean {
    return this.payModalInfo && this.payModalInfo.description && this.payModalInfo.description.length > 0;
  }

  public closePayModal(): void {
    this.isShowPayModal = false;
  }

  public stopProp(event): void {
    event.stopPropagation();
  }

  public getPayName(payCode: string): any[] {
    switch (payCode) {
      case 'assist': return ['Оплатить с помощью Ассист', this.payIcons.assist];
      case 'ipay_mts': return ['Оплатить через IPay МТС', this.payIcons.mts, this.payIcons.ipay];
      case 'ipay_life': return ['Оплатить через IPay Life:)', this.payIcons.life, this.payIcons.ipay];
      case 'ipay_erip': return ['Оплатить через IPay ЕРИП', this.payIcons.erip, this.payIcons.ipay];
      case 'yandex': return ['Оплатить с помощью системы Яндекс.Касса', this.payIcons.yandex];
      default: break;
    }
  }

  private loadPayWays(): void {
    this.authService.getTariffs().then(t => this.payWays = t.pay_sys);
  }

  ngOnDestroy() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }
}
