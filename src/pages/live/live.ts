import { IonicPage, NavParams } from 'ionic-angular';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { ChannelsState } from '../../redux/channels/channels.state';
import { HeaderButton, HeaderButtonType } from '../../models/header';
import { Genre } from './../../models/category';
import { Channel } from './../../models/channel';

@IonicPage()
@Component({
    selector: 'page-live',
    templateUrl: './live.html'
})

export class LivePage {

    public channelsFeed: Channel[] = [];
    public headerButtons: HeaderButtonType[] = [HeaderButton.BUTTON_MENU, HeaderButton.BUTTON_FILTER, HeaderButton.BUTTON_FAVORITE, HeaderButton.BUTTON_SEARCH];
    public categories: Genre[] = [];
    public activeGenreId: number;

    private channels:Channel[] = [];
    private skip:number = 0;
    private loadPart:number = 20; // Количество разово загружаемых каналов на отображение в ленте
    private filteredChannels = [];
    private channelsStoreSubscriber: Subscription;

    constructor(private channelsStore: Store<ChannelsState>, private navParams: NavParams) {}

    ngOnInit(): void {
        this.channelsStoreSubscriber = this.channelsStore.select('channelsData').subscribe(res => {
            this.channels = res.channels;
            this.categories = res.categories;
            this.filteredChannels = res.channels;
            this.activeGenreId = this.navParams.get('genre_id');
            if (this.activeGenreId) {
              this.changeFilter(this.activeGenreId);
            } else {
              this.loadFeed();
            }
        });
    }

    public doInfinite(e): void { // Метод бесконечного скролла
        this.loadFeed();
        e.complete();
    }

    public changeFilter(id: number): void {
        this.skip = 0;
        this.channelsFeed = [];
        this.filteredChannels = this.channels.filter(channel => {
           return channel.genres.find(genre => genre == id);
        });
        if (id == 0) {
            this.filteredChannels = this.channels;
        }
        this.loadFeed();
    }

    private loadFeed(): void {
        this.channelsFeed.push(...this.filteredChannels.slice(this.skip, this.skip + this.loadPart));
        this.skip += this.loadPart;
    }

    ngOnDestroy() {
        if (this.channelsStoreSubscriber) this.channelsStoreSubscriber.unsubscribe();
    }

}
