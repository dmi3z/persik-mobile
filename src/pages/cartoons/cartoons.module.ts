import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CartoonsPage } from './cartoons';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CartoonsPage
  ],
  imports: [
    IonicPageModule.forChild(CartoonsPage),
    ComponentsModule
  ],
  exports: [
    CartoonsPage
  ]
})

export class CartoonsPageModule {}
