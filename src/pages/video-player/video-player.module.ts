import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from '../../components/components.module';
import { VideoPlayerPage } from './video-player';

@NgModule({
  declarations: [
    VideoPlayerPage
  ],
  imports: [
    IonicPageModule.forChild(VideoPlayerPage),
    ComponentsModule
  ],
  exports: [
    VideoPlayerPage
  ]
})

export class VideoPlayerPageModule {}
