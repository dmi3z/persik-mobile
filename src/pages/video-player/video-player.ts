import { WatchMemoryService } from './../../services/watch-memory.service';
import { Subscription } from 'rxjs/Subscription';
import { ContentType } from './../../models/vod';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { PlayerComponent } from './../../components/player/player';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { PlayerEvents, ControlEvents, VideoControlEventInfo } from '../../models/player';
import { HeaderButtonType, HeaderButton } from '../../models/header';
import { ContentInMemory } from '../../models/memory';

@IonicPage()
@Component({
    selector: 'page-video-player',
    templateUrl: './video-player.html'
})

export class VideoPlayerPage {

    @ViewChild('pl') video: PlayerComponent;

    public duration: number = 0;
    public currentTime: number = 0;
    public isPlaying: boolean = false;
    public headerButtons: HeaderButtonType[] = [HeaderButton.BUTTON_BACK, HeaderButton.BUTTON_FAVORITE, HeaderButton.BUTTON_SEARCH];
    public currentOrientation: string;
    public isCanPlay: boolean = false;

    private id: number;
    private type: ContentType;
    private updateInterval: any;

    private screenOrientationSubscriber: Subscription;
    private playerEventsSubscriber: Subscription;

    constructor(private navParams: NavParams, private screenOrientation: ScreenOrientation, private watchMemoryService: WatchMemoryService) {}

    ngOnInit() {
        this.id = this.navParams.get('id');
        this.type = this.navParams.get('type');

        this.video.play(this.id, this.type);
        this.initPlayerBehaviors();

        this.currentOrientation = this.screenOrientation.type;

        this.screenOrientation.onChange().subscribe(() => {
            this.currentOrientation = this.screenOrientation.type;
        });
    }

    private checkMemory() { // Проверка, есть ли в памяти просмотренное видео
      const time: number = this.watchMemoryService.getFromMemory(this.id).time;
      if ( time > 0) {
        this.video.seek(time);
      }
    }

    public onControlEvent(event: VideoControlEventInfo): void {                     // Подписка на события контрола
        switch (event.name) {
            case ControlEvents.CONTROL_PAUSE:
                this.video.pause();
                break;
            case ControlEvents.CONTROL_PLAY:
                this.video.resume();
                break;
            case ControlEvents.CONTROL_SEEK:
                this.video.seek(event.value);
                break;
            default:
                break;
        }
    }

    public get isLandscape(): boolean {
        return this.screenOrientation.type.includes('landscape');
    }

    private initPlayerBehaviors() {                            // Подписка на события плеера
        this.playerEventsSubscriber = this.video.playerEvents.subscribe((event) => {
            switch (event) {
                case PlayerEvents.PLAYER_READY:
                    this.duration = this.video.duration;
                    this.startCurrentTimeController();
                    this.checkMemory();
                    this.isPlaying = true;
                    this.isCanPlay = true;
                    break;
                case PlayerEvents.PLAYER_PAUSE:
                    this.stopCurrentTimeController();
                    this.isPlaying = false;
                    break;
                case PlayerEvents.PLAYER_PLAY:
                    this.startCurrentTimeController();
                    this.isPlaying = true;
                    break;
                case PlayerEvents.PLAYER_ERROR_SUBSCRIPTION:
                    this.stopCurrentTimeController();
                    this.isCanPlay = false;
                    break;
                case PlayerEvents.PLAYER_ERROR_LOGIN:
                    this.stopCurrentTimeController();
                    this.isCanPlay = false;
                    break;
                default:
                    break;
            }
        });
    }

    private startCurrentTimeController(): void {
        this.updateInterval = setInterval(() => {
            this.currentTime = this.video.currentTime;
        }, 1000);
    }

    private stopCurrentTimeController(): void {
        clearInterval(this.updateInterval);
    }

    ngOnDestroy() {
      if (this.duration - this.currentTime >= 5) {
        this.watchMemoryService.setInMemory(new ContentInMemory(this.id, this.currentTime));
      } else {
        this.watchMemoryService.setInMemory(new ContentInMemory(this.id, 0));
      }

        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        if (this.screenOrientationSubscriber) this.screenOrientationSubscriber.unsubscribe();
        if (this.playerEventsSubscriber) this.playerEventsSubscriber.unsubscribe();
        this.video.stop();
    }

}
