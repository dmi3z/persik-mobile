import { Genre } from './../../models/category';
import { Store } from '@ngrx/store';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { DataService } from './../../services/data.service';
import { Subscription } from 'rxjs/Subscription';
import { VodSection, ContentName, FeaturedVideosTape } from './../../models/vod';
import { VodState } from './../../redux/vod/vod.state';
import { Channel, Banner } from '../../models/index';
import { ChannelsState } from '../../redux/channels/channels.state';
import { Platform } from 'ionic-angular';
import { FeaturedChannelsTape } from '../../models/channel';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  @ViewChild('slickModal') banners: ElementRef;

  public slideConfig: any = {
    "slidesToShow": 1,
    "slidesToScroll": 1,
    "arrows": false,
    "dots": true,
    "centerMode": true,
    "centerPadding": '15%',
    "responsive": [
      {
        "breakpoint": 450,
        "settings": {
          "centerPadding": '0px'
        }
      }
    ]
  };

  public featuredChannels: Channel[] = [];
  public featuredChannelsTitle: string = 'Популярные каналы';
  public bannersData: Banner[] = [];
  public featuredVod: VodSection[] = [];

  public realOrientation: string;
  private isLock: boolean = false;

  private channelsStoreSubscriber: Subscription;
  private vodStoreSubscriber: Subscription;
  private screenOrientationSubscriber: Subscription;
  public loadingStep = 0;

  public featuredChannelsTapes: FeaturedChannelsTape[] = [];
  public featuredFilmsTapes: FeaturedVideosTape[] = [];
  public featuredSeriesTapes: FeaturedVideosTape[] = [];
  public featuredCartoonsTapes: FeaturedVideosTape[] = [];
  public featuredTvshowsTapes: FeaturedVideosTape[] = [];

  private updateTimer: any;
  private isLoadDisabled: boolean;

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    private channelsStore: Store<ChannelsState>,
    private vodStore: Store<VodState>,
    private dataService: DataService,
    private screenOrientation: ScreenOrientation
  ) { }

  ngOnInit() {
    this.loadBanners();
    this.channelsStoreSubscriber = this.channelsStore.select('channelsData').subscribe(res => {
      if (res.channels.length > 0 && res.featured_channels.length > 0) {
        this.featuredChannels = res.channels.filter(channel => {
          return res.featured_channels.filter(featured => featured.channel_id == channel.channel_id).length == 1;
        });
      }
    });

    this.vodStoreSubscriber = this.vodStore.select('vodCategories').subscribe(res => {
      this.featuredVod = res.featured.sections;
    });

    this.loadChannelsForFeatured();
  }

  public loadNext(): void {
    this.loadingStep += 1;
    switch (this.loadingStep) {
      case 1:
        this.loadFilmsTapes();
        break;
      case 2:
        this.loadSeriesTapes();
        break;
      case 3:
        this.loadCartoonsTapes();
        break;
      case 4:
        this.loadTvshowsTapes();
        break;
      default:
        break;
    }
  }

  public getRedirect(page: string, genre_id?: number) {
    const genre = genre_id ? genre_id : 0;
    return {page, genre};
  }

  private loadChannelsForFeatured(): void {
    const genres: Genre[] = [
      {
        id: 602,
        name: 'Белорусские',
        name_en: 'belorusskie',
        is_main: true
      },
      {
        id: 99,
        name: 'Кино и сериалы',
        name_en: 'films-i-serialy',
        is_main: true
      },
      {
        id: 105,
        name: 'Спортивные',
        name_en: 'pro-sport',
        is_main: true
      },
      {
        id: 98,
        name: 'Детские',
        name_en: 'detskie',
        is_main: true
      }
    ];
    this.dataService.getChannelsForFeatured(genres).then(res => {
      this.featuredChannelsTapes = res;
    });
  }

  private loadFilmsTapes(): void {
    this.vodStore.select('vodCategories').subscribe(cats => {
      const g_films_ids: number[] = [1007, 1008, 1023, 1009, 1010, 1011, 1012, 1013, 1014];
      const allgenres_t: Genre[][] = cats.categories.map(cat => cat.genres);
      const allgenres: Genre[] = [];
      for (let i = 0; i < allgenres_t.length; i++) {
        allgenres.push(...allgenres_t[i]);
      }
      this.loadFilmsWithCategory(allgenres.filter(genre => g_films_ids.some(gfi => +gfi === +genre.id)));
    });
  }

  private loadFilmsWithCategory(genres: Genre[]): void {
    for (let i = 0; i < genres.length; i++) {
      this.dataService.loadVideoContent(1, genres[i].id, 10).then(v => {
        this.featuredFilmsTapes.push({
          genre_id: genres[i].id,
          title: genres[i].name,
          videos: v.videos
        });
      });
    }
  }

  private loadSeriesTapes(): void {
    this.vodStore.select('vodCategories').subscribe(cats => {
      const g_series_ids: number[] = [1015, 1024];
      const allgenres_t: Genre[][] = cats.categories.map(cat => cat.genres);
      const allgenres: Genre[] = [];
      for (let i = 0; i < allgenres_t.length; i++) {
        allgenres.push(...allgenres_t[i]);
      }
      this.loadSeriesWithCategory(allgenres.filter(genre => g_series_ids.some(gsi => +gsi === +genre.id)));
    });
  }

  private loadSeriesWithCategory(genres: Genre[]): void {
    for (let i = 0; i < genres.length; i++) {
      this.dataService.loadVideoContent(2, genres[i].id, 10).then(v => {
        this.featuredSeriesTapes.push({
          genre_id: genres[i].id,
          title: genres[i].name,
          videos: v.videos
        });
      });
    }
  }

  private loadCartoonsTapes(): void {
    this.vodStore.select('vodCategories').subscribe(cats => {
      const g_cartoons_ids: number[] = [1016];
      const allgenres_t: Genre[][] = cats.categories.map(cat => cat.genres);
      const allgenres: Genre[] = [];
      for (let i = 0; i < allgenres_t.length; i++) {
        allgenres.push(...allgenres_t[i]);
      }
      this.loadCartoonsWithCategory(allgenres.filter(genre => g_cartoons_ids.some(gci => +gci === +genre.id)));
    });
  }

  private loadCartoonsWithCategory(genres: Genre[]): void {
    for (let i = 0; i < genres.length; i++) {
      this.dataService.loadVideoContent(3, genres[i].id, 10).then(v => {
        this.featuredCartoonsTapes.push({
          genre_id: genres[i].id,
          title: genres[i].name,
          videos: v.videos
        });
      });
    }
  }


  private loadTvshowsTapes(): void {
    this.vodStore.select('vodCategories').subscribe(cats => {
      const g_tvshows_ids: number[] = [655, 447, 449, 96, 892];
      const allgenres_t: Genre[][] = cats.categories.map(cat => cat.genres);
      const allgenres: Genre[] = [];
      for (let i = 0; i < allgenres_t.length; i++) {
        allgenres.push(...allgenres_t[i]);
      }
      this.loadTvshowsWithCategory(allgenres.filter(genre => g_tvshows_ids.some(gti => +gti === +genre.id)));
    });
  }

  private loadTvshowsWithCategory(genres: Genre[]): void {
    for (let i = 0; i < genres.length; i++) {
      this.dataService.loadVideoContent(4, genres[i].id, 10).then(v => {
        this.featuredTvshowsTapes.push({
          genre_id: genres[i].id,
          title: genres[i].name,
          videos: v.videos
        });
      });
    }
  }

  public toggleOrientation(): void {
    if (this.isLock) {
      this.screenOrientation.unlock();
      this.realOrientation = this.realOrientation + ' (unlocked)';
      this.isLock = false;
    } else {
      this.screenOrientation.lock('portrait');
      this.realOrientation = this.realOrientation + ' (locked)';
      this.isLock = true;
    }
  }

  public get isHaveFeaturedChannels(): boolean {
    return this.featuredChannels.length > 0;
  }

  public get isHaveBanners(): boolean {
    return this.bannersData.length > 0;
  }

  public bannerRedirect(banner: Banner): void {
    switch (banner.element_type) {
      case 'tvshow':
        this.dataService.getTvshowInfo(banner.element_id.toString()).then(data => {
          if (data) {
            this.dataService.getVideoInfo(+data.video_id).then(res => {
              this.navCtrl.push('VodInfoPage', {
                info: res,
                type: ContentName.TV,
                video_id: res.video_id,
                tvshow_id: banner.element_id
              });
            });
          }
        });
        break;

      case 'video':
        this.dataService.getVideoInfo(+banner.element_id).then(data => {
          this.navCtrl.push('VodInfoPage', {
            info: data,
            type: ContentName.VIDEO,
            video_id: banner.element_id,
            tvshow_id: null
          });
        });
        break;

      case 'channel':
        this.navCtrl.push('ChannelPlayerPage', { channel_id: banner.element_id });
        break;

      case 'site':
        if (banner.url) {
          window.open(banner.url, "_blank");
        }
        break;

      default: break;

    }
  }

  public doInfinite(e) {
    if (!this.isLoadDisabled) {
      this.loadNext();
      this.isLoadDisabled = true;
    }
    clearTimeout(this.updateTimer);
    this.updateTimer = setTimeout(() => {
      this.isLoadDisabled = false;
    }, 500);
    e.complete();
  }

  private loadBanners() {
    this.dataService.getBanners().then(data => {
      this.bannersData = data;
    });
  }

  ngOnDestroy() {
    if (this.channelsStoreSubscriber) this.channelsStoreSubscriber.unsubscribe();
    if (this.vodStoreSubscriber) this.vodStoreSubscriber.unsubscribe();
    if (this.screenOrientationSubscriber) this.screenOrientationSubscriber.unsubscribe();
  }

}

