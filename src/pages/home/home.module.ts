import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { ComponentsModule } from '../../components/components.module';
import { SlickModule } from 'ngx-slick';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    ComponentsModule,
    SlickModule.forRoot()
  ],
  exports: [
    HomePage
  ]
})

export class HomePageModule {}
