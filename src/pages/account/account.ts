import { TariffItemComponent } from './../../components/subscriptions/tariff-item/tariff-item';
import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-account',
    templateUrl: './account.html'
})

export class AccountPage {

    public isAccountSelected: boolean = false;

    constructor(private navParams: NavParams) {}

    ngOnInit() {
        const selectedPage: AccountPageType = this.navParams.get('page');
        if (selectedPage == 'subscription') {
            this.isAccountSelected = false;
        }
    }

    selectPoint(name: string): void {
        switch (name) {
            case 'account':
                this.isAccountSelected = true;
                break;
            case 'subscr':
                this.isAccountSelected = false;
                break;
            default:
                this.isAccountSelected = null;
                break;
        }
    }

    ngOnDestroy() {
        TariffItemComponent.selectedTariffId = null;
        TariffItemComponent.selectedOptionId = null;
    }
}

export type AccountPageType = 'account' | 'subscription';