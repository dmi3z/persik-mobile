import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { VodSection, ContentName } from './../../models/vod';
import { Store } from '@ngrx/store';
import { Channel } from './../../models/channel';
import { SearchData } from './../../models/search';
import { DataService } from './../../services/data.service';
import { Component } from '@angular/core';
import { IonicPage, LoadingController, Loading, Platform } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { ChannelsState } from '../../redux/channels/channels.state';

@IonicPage()
@Component({
    selector: 'page-search',
    templateUrl: './search.html'
})

export class SearchPage {

    public text: string = '';
    public isShowVideos: boolean = false;
    public isShowTvshows: boolean = false;
    public isNothingToShow: boolean = false;

    public channels: Channel[] = [];
    public videos: VodSection;
    public tvshows: VodSection;

    private channelsStoreSubscriber: Subscription;
    private spinner: Loading;

    constructor(
      private dataService: DataService,
      private channelStore: Store<ChannelsState>,
      public loadingCtrl: LoadingController,
      private platform: Platform,
      private screenOrientation: ScreenOrientation
    ) {}

    public getItems(): void {
        if (this.text.length >= 2) {
            this.spinner = this.loadingCtrl.create({
              spinner: 'circles'
            });
            this.spinner.present();
            this.dataService.searchContent(this.text).then(res => {
              console.log(res);
              this.renderSearchInfo(res);
            });
        } else {
          if (this.spinner) this.spinner.dismiss();
        }
    }

    public get isLandscape(): boolean {
      return this.screenOrientation.type.includes('landscape');
    }

    public get isTablet(): boolean {
      return this.platform.is('tablet');
    }

    private renderSearchInfo(result: SearchData): void {
        this.channels = [];

        if (result.channels.length > 0) {
            result.channels.forEach(ch => {
                this.channelsStoreSubscriber = this.channelStore.select('channelsData').subscribe(data => {
                    const findChannel: Channel = data.channels.find(d_ch => d_ch.channel_id == ch.channel_id);
                    if (findChannel) this.channels.push(findChannel);
                });
            });
        }
        if (result.videos.length > 0) {
            this.isShowVideos = true;
            this.videos = {
                type: ContentName.VIDEO,
                videos: result.videos
            }
        }

        if (result.tvshows.length > 0) {
            this.isShowTvshows = true;
            this.tvshows = {
                type: ContentName.TV,
                videos: result.tvshows
            }
        }

        this.isNothingToShow = false;
        if (result.channels.length == 0 && result.videos.length == 0 && result.tvshows.length == 0) {
            this.isNothingToShow = true;
            this.isShowTvshows = false;
            this.isShowVideos = false;
        };
        this.spinner.dismiss();
    }

    public clearItems() {
        this.isNothingToShow = false;
        this.channels = [];
        this.isShowVideos = false;
        this.isShowTvshows = false;
    }

    public get isHaveChannels(): boolean {
        return this.channels.length > 0;
    }

    ngOnDestroy() {
        if (this.channelsStoreSubscriber) this.channelsStoreSubscriber.unsubscribe();
    }

}
