import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http'; // HttpClient
import { StoreModule } from '@ngrx/store';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { DeviceMotion } from '@ionic-native/device-motion';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { PersikMobileApp } from './app.component';
import { ModalComponent} from '../components/index';

import { HomePageModule } from '../pages/home/home.module';
import { FilmsPageModule } from '../pages/films/films.module';
import { CartoonsPageModule } from '../pages/cartoons/cartoons.module';
import { SearchPageModule } from '../pages/search/search.module';
import { FavoritePageModule } from '../pages/favorite/favorite.module';
import { CoursesPageModule } from '../pages/courses/courses.module';
import { LivePageModule } from '../pages/live/live.module';
import { SeriesPageModule } from '../pages/series/series.module';
import { ShowsPageModule } from '../pages/shows/shows.module';
import { VodInfoPageModule } from '../pages/vod-info/vod-info.module';
import { TvPageModule } from '../pages/tv/tv.module';
import { LoginPageModule } from '../pages/login/login.module';
import { AccountPageModule } from '../pages/account/account.module';
import { ModalController } from '../services/modal.controller';
import { VideoPlayerPageModule } from '../pages/video-player/video-player.module';
import { ChannelReviewPageModule } from '../pages/channel-review/channel-review.module';
import { BillingConfirmPageModule } from '../pages/billing-confirm/billing-confirm.module';
import { ChannelPlayerPageModule } from '../pages/channel-player/channel-player.module';
import { CoursePaymentPageModule } from '../pages/course-payment/course-payment.module';

import { channelsReducer } from '../redux/channels/channels.reducer';
import { DataService } from '../services/data.service';
import { ImageService } from '../services/image.service';
import { TimeService } from '../services/time.service';
import { AuthService } from '../services/auth.service';
import { FavoriteService } from '../services/favorite.service';
import { vodReducer } from '../redux/vod/vod.reducer';
import { tvshowsReducer } from '../redux/tvshows/tvshows.reducer';
import { ParamInterceptor } from '../services/api.interceptor';
import { favoritesReducer } from '../redux/favorite/favorite.reducer';
import { OrientationService } from '../services/orientation.service';
import { WatchMemoryService } from '../services/watch-memory.service';
import { PladformPlayerPageModule } from '../pages/pladform-player/pladform-player.module';

/* import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader'; */

@NgModule({
  declarations: [
    PersikMobileApp,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(PersikMobileApp),
    HttpClientModule,
    HomePageModule,
    FilmsPageModule,
    CartoonsPageModule,
    SearchPageModule,
    FavoritePageModule,
    LivePageModule,
    SeriesPageModule,
    ShowsPageModule,
    VodInfoPageModule,
    TvPageModule,
    CoursesPageModule,
    LoginPageModule,
    AccountPageModule,
    VideoPlayerPageModule,
    ChannelReviewPageModule,
    BillingConfirmPageModule,
    ChannelPlayerPageModule,
    CoursePaymentPageModule,
    PladformPlayerPageModule,
    StoreModule.forRoot({channelsData: channelsReducer, vodCategories: vodReducer, tvshowsData: tvshowsReducer, favoritesData: favoritesReducer}),
    /* TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }) */
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    PersikMobileApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DataService,
    WatchMemoryService,
    OrientationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ParamInterceptor,
      multi: true
    },
    ImageService,
    TimeService,
    AuthService,
    ModalController,
    FavoriteService,
    ScreenOrientation,
    DeviceMotion,
    UniqueDeviceID,
    GoogleAnalytics,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

/* export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
} */
