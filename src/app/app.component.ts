import { ShowsPage } from './../pages/shows/shows';
import { SeriesPage } from './../pages/series/series';
import { CartoonsPage } from './../pages/cartoons/cartoons';
import { FilmsPage } from './../pages/films/films';
import { TvPage } from './../pages/tv/tv';
import { LivePage } from './../pages/live/live';
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
// import { TranslateService } from '@ngx-translate/core';
import { FavoritePage } from './../pages/favorite/favorite';
import { SearchPage } from './../pages/search/search';
import { Subscription } from 'rxjs/Subscription';

import { HomePage } from '../pages';
import { SimpleHeaderComponent } from '../components/simple-header/simple-header';
import * as moment from 'moment';

import { AuthService } from './../services/auth.service';
import { DataService } from './../services/data.service';
import { FavoriteService } from '../services/favorite.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

@Component({
  templateUrl: 'app.html'
})

export class PersikMobileApp {

  @ViewChild(Nav) nav: Nav;

  public rootPage: any = HomePage;
  public menuPages: MenuPage[];

  private navigationIcons = {
    menu: '../assets/navigation-icons/bars-solid.svg',
    favorites: '../assets/navigation-icons/bookmark-regular.svg',
    search: '../assets/navigation-icons/search-solid.svg',
    home: '../assets/navigation-icons/home-solid.svg',
    live: '../assets/navigation-icons/live.png',
    film: '../assets/navigation-icons/film.png',
    multfilm: '../assets/navigation-icons/mult.png',
    serial: '../assets/navigation-icons/serial.png',
    stream: '../assets/navigation-icons/pere.png',
    tv: '../assets/navigation-icons/tv.png',
    courses: '../assets/navigation-icons/courses.png'
  };

  private loginControlSubscriber: Subscription;

  constructor(
      private platform: Platform,
      statusBar: StatusBar,
      splashScreen: SplashScreen,
      public toastCtrl: ToastController,
      private dataService: DataService,
      private authService: AuthService,
      private favoriteService: FavoriteService,
      private ga: GoogleAnalytics,
      private screenOrientation: ScreenOrientation
      // private translateService: TranslateService,
    ) {
    platform.ready().then(() => {

            // Translate ---
            /* translateService.addLangs(["ru"]);
            translateService.setDefaultLang('ru');
            translateService.use('ru');  */

      statusBar.styleDefault();
      splashScreen.hide();

      let lastTimeBackPress = 0;
      const timePeriodToExit = 2000;

      platform.registerBackButtonAction(() => {
        let view = this.nav.getActive();
        if (view.instance instanceof HomePage) {
          if (new Date().getTime() - lastTimeBackPress < timePeriodToExit){
            platform.exitApp();
          } else {
            let toast = toastCtrl.create({
              message: "Для выхода из приложения нажмите кнопку 'Назад' еще раз",
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
            lastTimeBackPress = new Date().getTime();
          }
        } else {
          if (this.nav.canGoBack()) {
            if (view.instance instanceof SearchPage || view.instance instanceof FavoritePage) {
              this.nav.popToRoot({animate: true, direction: 'forward'});
            } else {
              this.nav.pop();
            }
            SimpleHeaderComponent.clearAllButtonsActive();
          } else {
            this.nav.setRoot(HomePage);
          }
        }
      });

      // Google Analytics ---
      this.ga.startTrackerWithId('UA-129259070-1').then(() => {
        this.ga.trackView('home');
      }).catch(e => console.log(e));
    });
  }

  ngOnInit(): void {
    moment.locale('ru');
    if (this.platform.is('mobile')) {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
    this.authService.loadUuid(); // Получение уникального id устройства

    this.loginControlSubscriber = this.authService.loginStateController.subscribe(() => { // Подписка на изменение состояния пользователя (авторизован или нет)
      this.menuPages = this.getMenuButtons();
      this.dataService.loadChannels();
      this.loadFavorite();
    });

    this.menuPages = this.getMenuButtons();
    this.loadFavorite();
    this.dataService.loadChannels();
    this.dataService.loadFeatureChannels();
    this.dataService.loadChannelCategories();
    this.dataService.loadVodCategories();
    this.dataService.loadVodFeatured();
  }

  public isActiveNav(page: MenuPage): boolean {
    if (this.nav.getActive()) {
      return page.name == this.currentRouteName;
    }
    return false;
  }

  public goTo(page: MenuPage): void {
    if (!this.isActiveNav(page)) {
      if (page.name == 'HomePage') {
        this.nav.setRoot(page.name);
      } else {
        this.nav.push(page.name);
      }
      SimpleHeaderComponent.clearAllButtonsActive();
    }
  }

  private loadFavorite(): void {
    if (this.authService.isLogin) {
      this.favoriteService.loadFavorite();
    }
  }

  private getMenuButtons(): MenuPage[] {
    return [
      {title: 'Главная',               img: this.navigationIcons.home,     name: 'HomePage'},
      {title: 'ТВ: Сейчас в эфире',    img: this.navigationIcons.live,     name: 'LivePage'},
      {title: 'ТВ: Программа передач', img: this.navigationIcons.tv,       name: 'TvPage'},
      {title: 'Фильмы',                img: this.navigationIcons.film,     name: 'FilmsPage'},
      {title: 'Мультфильмы',           img: this.navigationIcons.multfilm, name: 'CartoonsPage'},
      {title: 'Сериалы',               img: this.navigationIcons.serial,   name: 'SeriesPage'},
      {title: 'Передачи',              img: this.navigationIcons.stream,   name: 'ShowsPage'},
      {title: 'Курсы',                 img: this.navigationIcons.courses,  name: 'CoursesPage'},
      {title: 'Личный кабинет',        icon: 'person',                     name: this.authService.isLogin ? 'AccountPage' : 'LoginPage'}
    ];
  }

  private get currentRouteName(): string { // Ужаснее метода я еще не писал, но без него получить имя текущей страницы в production mode
    const view: any = this.nav.getActive();
    if (view.instance instanceof HomePage) return 'HomePage';
    if (view.instance instanceof LivePage) return 'LivePage';
    if (view.instance instanceof TvPage) return 'TvPage';
    if (view.instance instanceof FilmsPage) return 'FilmsPage';
    if (view.instance instanceof CartoonsPage) return 'CartoonsPage';
    if (view.instance instanceof SeriesPage) return 'SeriesPage';
    if (view.instance instanceof ShowsPage) return 'ShowsPage';
    return 'AnyPage';
  }

  ngOnDestroy() {
    if (this.loginControlSubscriber) this.loginControlSubscriber.unsubscribe();
  }
}

interface MenuPage {
  title: string;
  img?: string;
  name: string;
  icon?: string;
}
